.PHONY: all clean compile deps distclean test run

REBAR = ./rebar

all: deps compile

compile: deps
	@$(REBAR) compile

deps:
	@$(REBAR) get-deps

clean:
	@$(REBAR) clean

distclean: clean
	@$(REBAR) delete-deps

test:
	ERL_FLAGS="-config app" $(REBAR) eunit

run:
	erl -pa deps/*/ebin ebin -rsh ssh -config app.config -eval 'code:load_file(pourse)'

