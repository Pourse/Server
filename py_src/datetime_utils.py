﻿import calendar
import jdatetime
import locale
import datetime
import pytz


abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
abbr_to_num['July'] = 7
abbr_to_num['Sept'] = 9
abbr_to_num['June'] = 6
abbr_to_num[u'فروردين'] = 1
abbr_to_num[u'فروردین'] = 1
abbr_to_num[u'ارديبهشت'] = 2
abbr_to_num[u'اردیبهشت'] = 2
abbr_to_num[u'خرداد'] = 3
abbr_to_num[u'تير'] =4
abbr_to_num[u'تیر'] =4
abbr_to_num[u'مرداد'] = 5
abbr_to_num[u'شهريور'] = 6 
abbr_to_num[u'شهریور'] = 6 
abbr_to_num[u'مهر'] = 7
abbr_to_num[u'آبان'] = 8
abbr_to_num[u'آذر'] = 9
abbr_to_num[u'دي'] = 10
abbr_to_num[u'دی'] = 10
abbr_to_num[u'بهمن'] = 11
abbr_to_num[u'اسفند'] = 12


def persian_date_to_utc(year, month, day, hour, minute):
    old_date_and_time = '%s-%s-%s %s:%s:00' % (year, month, day, hour, minute)

    local = pytz.timezone('Asia/Tehran')
    naive = jdatetime.datetime.strptime(old_date_and_time, "%Y-%m-%d %H:%M:%S")
    native = naive.togregorian()
    local_dt = local.localize(native, is_dst=None)

    utc_dt = local_dt.astimezone(pytz.utc)
    ts = utc_dt.strftime("%s")
    return int(ts)


def gregorian_to_utc(year, month, day, hour, minute):
    old_date_and_time = '%s-%s-%s %s:%s:00' % (year, month, day, hour, minute)

    local = pytz.timezone('Asia/Tehran')
    naive = datetime.datetime.strptime(old_date_and_time, "%Y-%m-%d %H:%M:%S")
    local_dt = local.localize(naive, is_dst=None)

    utc_dt = local_dt.astimezone(pytz.utc)
    ts = utc_dt.strftime("%s")
    return int(ts)


def jutcnow():
    return jdatetime.datetime.utcnow()
