# -*- coding: utf-8 -*-

import urllib, urllib2
import json
from bs4 import BeautifulSoup ,Tag
import requests
from datetime_utils import abbr_to_num, persian_date_to_utc, gregorian_to_utc, jutcnow


def scrape_categories():
    categories = {}

    quote_page = "http://new.tse.ir/json/Listing/ListingByGroup1.json"
    page = urllib2.urlopen(quote_page)
    data = json.loads(page.read().decode('utf-8'))

    industry_counter = 0
    for item in data['data']:
        industry = data['data'][industry_counter]['i']
        index_counter = 0
        
        for index in data['data'][industry_counter]['z']:
            sub_industry = data['data'][industry_counter]['z'][index_counter]['z']
            object_counter = 0

            for obj in sub_industry:
                properties = sub_industry[object_counter]
                name = properties['t']
                if name.endswith(' '):
                    name = name[:-1]
                symbol = properties['sy']
                ic = properties['ic']
                object_counter = object_counter + 1

                if not industry in categories:
                    categories[industry] = []
                categories[industry].append({'name': name, 'symbol': symbol, 'ic': ic})

            index_counter += 1
        industry_counter += 1

    return json.dumps(dict(categories=categories))


def scrape_stock_general_info(ic):
    url = "http://new.tse.ir/en/json/Instrument/info_%s.json?_" % ic
    page = urllib2.urlopen(url)
    all_data = json.loads(page.read().decode('utf-8'))

    time_and_date = all_data['header'][1]['time'].split('-')
    date = time_and_date[0].split()
    time = time_and_date[1].split(':')

    if len(date) < 3 or len(time) < 2:
        return json.dumps(dict())

    year = int(date[2])
    month = int(abbr_to_num[date[1]])
    day = int(date[0])
    hour = int(time[0])
    minute = int(time[1])
    last_time = gregorian_to_utc(year, month, day, hour, minute)

    info = dict(
        closing_price           = int(all_data['mainData'][0]['sa'].replace(',', '')),
        closing_price_change    = float(all_data['mainData'][0]['sr'].split()[0].replace('%', '').replace('(', '').replace(')', '')),
        price_range_min         = int(all_data['mainData'][2]['d'].replace(',', '')),
        price_range_max         = int(all_data['mainData'][2]['u'].replace(',', '')),
        first_price             = int(all_data['mainData'][6].replace(',', '')),
        last_day_price          = int(all_data['mainData'][8].replace(',', '')),
        number_of_trades        = int(all_data['mainData'][13].replace(',', '')),
        value_of_trades         = int(float(all_data['mainData'][18].split()[0].replace(',', '.')) * 1000000000),
        number_of_shares        = int(float(all_data['mainData'][7].split()[0].replace(',', '.')) * 1000000),
        last_trade_price        = int(all_data['header'][1]['am'].replace(',', '')),
        last_trade_price_change = float(all_data['header'][1]['dar'].split()[0].replace('%', '').replace('(', '').replace(')', '')),
        last_trade_time         = last_time
    )

    return json.dumps(info)

    
def scrape_comments(to_date, from_index):
   
    def value_maker(whole_comment):
        text = whole_comment['clearBody']
        source = 'sahamyab'

        word_list = text.split()
        hashtag_words = []
        for word in word_list:
            if '#' in word:
                hashtag_words.append(word.replace('#', ''))

        time_and_date_list = whole_comment['modifydate'].split()
        date_list = time_and_date_list[0].split('/')
        time_list = time_and_date_list[1].split(':')

        year = date_list[0]
        month = date_list[1]
        day = date_list[2]
        hour = time_list[0]
        minute = time_list[1]
        time = persian_date_to_utc(year, month, day, hour, minute)

        res = dict(
            symbols = hashtag_words,
            time    = time,
            source  = source,
            text    = text 
        )
        return res

    comments = []
    url = "https://www.sahamyab.com/stocktwits?"
    params = urllib.urlencode(
        {
            'p_p_id': 'EXT_BOURSE_STOCKTWITS',
            'p_p_lifecycle': '2',
            'p_p_state': 'normal',
            'p_p_mode': 'view',
            'p_p_cacheability': 'cacheLevelPage',
            'p_p_col_id': 'column-1',
            '_EXT_BOURSE_STOCKTWITS_type': 'findComments',
            'ctype': '',
            'showTypeAccessories': 'SHARP',
            'toDate': to_date,
            'fromIndex': from_index,
            'sort': 'createDateTime'
        }
    )

    link = url + params
    page = urllib2.urlopen(link)
    all_data = json.loads(page.read().decode('utf-8'))['result']

    for data in all_data:
        value = value_maker(data)
        comments.append(value)
    
    return json.dumps(dict(comments=comments[::-1]))


def scrape_news_list(count):
    params = {
        'rpp': str(count)
    }
    url = 'http://www.boursenews.ir/fa/archive'

    r = requests.get(url, params=params)
    page = r.text
    soup = BeautifulSoup(page, 'html.parser')

    all_tags = soup.findAll("a", { "class" : "title5" })
    check_list = []
    news_list = []
    for index in range(len(all_tags)):
        item = {}
        news = all_tags[index].contents[0].rstrip().lstrip()
        if not news in check_list:
            check_list.append(news)
            item['title'] = news
            item['url'] = all_tags[index]['href']
            news_list.append(item)

    return json.dumps(dict(news=news_list))


def scrape_news(url):

    def info_extract(isoup):
        tlist = []
        def info_extract_helper(inlist, count = 0):
            if(isinstance(inlist, list)):
                for q in inlist:
                    if(isinstance(q, Tag)):
                        info_extract_helper(q.contents, count + 1)
                    else:
                        extracted_str = q.strip()
                        if(extracted_str and (count > 1)):
                            tlist.append(extracted_str)
        info_extract_helper([isoup])
        return tlist

    link = "http://www.boursenews.ir" + url
    page = urllib2.urlopen(link)
    soup = BeautifulSoup(page.read(), 'html.parser')

    outer_text = soup.findAll("div", { "class" : 'body'})
    title_tag = soup.findAll("div", { "class" : "title"})
    title = title_tag[0].contents[1].contents[1].contents[0].rstrip().lstrip()
    source = 'boursenews'

    text = ''
    for index in range(len(info_extract(outer_text[0]))):
        text = text + info_extract(outer_text[0])[index]

    all_comment = soup.findAll("div", { "class" : 'comments'})
    comment_list = []
    for num in range (len(all_comment)):
        comment = all_comment[num].contents[1].contents[0].rstrip().lstrip()
        comment_list.append(comment)

    all_comment_date = soup.findAll("div", { "class" : 'comm_info_date'})
    comment_date_list = []
    for com_num in range(len(all_comment_date)):
        time_and_date_list = all_comment_date[com_num].contents[0].split('-')
        time_list = time_and_date_list[0].rstrip().lstrip().split(':')
        date_list = time_and_date_list[1].rstrip().lstrip().split('/')

        year = int(date_list[0])
        month = int(date_list[1])
        day = int(date_list[2])
        hour = int(time_list[0])
        minute = int(time_list[1])

        dict_comment_time_date = persian_date_to_utc(year, month, day, hour, minute)
        comment_date_list.append(dict_comment_time_date)

    comment_counter = 0
    external_comment_list = []
    for com in comment_list:
        external_comments = dict(
            source  = source,
            text    = comment_list[comment_counter],
            time    = comment_date_list[comment_counter]
        )
        comment_counter += 1
        external_comment_list.append(external_comments)

    total_date = soup.findAll("div", {"class": 'news_nav news_pdate_c'})[0].contents[1].split('-')
    total_new_date = total_date[0].rstrip().lstrip().split()
    total_new_time = total_date[1].rstrip().lstrip().split(':')

    hour = int(total_new_time[0])
    minute = int(total_new_time[1])
    day = int(total_new_date[0])
    year = int(total_new_date[2])
    month = abbr_to_num[total_new_date[1]]
    time = persian_date_to_utc(year, month, day, hour, minute)

    main_tag = soup.findAll("div", { "style" : 'direction: rtl;'})
    image_tag = main_tag[0].findAll('img', src=True)
    images = []
    for iterator in range (len(image_tag)):
        alink = image_tag[iterator].attrs['src']
        if not alink in images:
            if alink.startswith('/'):
                alink = "http://www.boursenews.ir" + alink
            images.append(alink)

    news = dict(
        title   = title,
        source  = source,
        text    = text,
        time    = time,
        images  = images,
        external_comments = external_comment_list[::-1]
    )
    return json.dumps(news)


def scrape_bourse_status():      
    quote_page = "http://new.tse.ir/json/HomePage/nazerMSG.json"
    page = urllib2.urlopen(quote_page)
    data = json.loads(page.read().decode('utf-8'))["miniSlider"]

    time = data[4]['h'].split(':')
    date = data[4]['date'].split()

    year = jutcnow().year
    month = int(abbr_to_num[date[1]])
    day = int(date[0])
    hour = int(time[0])
    minute = int(time[1])
    latest = persian_date_to_utc(year, month, day, hour, minute)

    general_infos = dict( 
        tedpix  = float(data[0]),
        volume  = int(data[1] * 1000000),
        number  = int(data[2]),
        value   = int(data[3]),
        time    = latest
    )
    return json.dumps(general_infos)
