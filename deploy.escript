#!/usr/bin/env escript
%%! -config app.config

main([]) ->
	load_modules(),
	start().



load_modules() ->
	Paths = lists:map(fun(X)-> filename:dirname(X) end, filelib:wildcard("*/**")),
	lists:foreach(fun(X) -> code:add_patha(X) end, Paths).


start() ->
	pourse:start(),
	loop().


loop() ->
	receive
		exit -> exit;
		upgrade ->
			pourse:upgrade_code(),
			loop()
	end.
