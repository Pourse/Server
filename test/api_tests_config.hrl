-ifndef(api_tests_config).
-define(api_tests_config, true).


-include("config.hrl").

-define(API_CONFIG(Headers), [
	{base_url, "http://localhost:" ++ integer_to_list(?env(api, port)) ++ "/api"},
	{headers , Headers ++ [
		{"Accept", "application/json"}
	]}
]).


-endif. % api_tests_config
