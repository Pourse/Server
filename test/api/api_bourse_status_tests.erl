-module(api_bourse_status_tests).

-import(api_user_tests, [
	signup/4,
	login/4,
	delete_user/1
]).

-import(pourse_datetime_utils, [
	utc/0,
	timestamp/1
]).

-include_lib("eunit/include/eunit.hrl").

-include("../api_tests_config.hrl").

-include("authenticator.hrl").
-include("data.hrl").
-include("api/api.hrl").
-include("api/info/bourse_status_info.hrl").
-include("data/bourse_status_data.hrl").



api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun get_info1/0
		]
	}.


setup() ->
	application:load(pourse),
	pourse_db_adapter:ensure_all_started(),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



create_status(Id, Tedpix, Volume, Number, Value, Latest, Timestamp) ->
	DB = pourse_db_adapter:new(),
	NewStatus = #bourse_status_data{
		id				= Id,
		tedpix			= Tedpix,
		volume			= Volume,
		number			= Number,
		value			= Value,
		latest			= Latest,
		timestamp		= Timestamp
	},
	DB:put(?BOURSE_STATUS_BUCKET, Id, NewStatus, "timestamp", NewStatus#bourse_status_data.timestamp),
	NewStatus.


update_last_status(Status) ->
	DB = pourse_db_adapter:new(),
	DB:put(?BOURSE_STATUS_BUCKET, <<"last">>, Status#bourse_status_data{id = <<"last">>}).


delete_status(Id) ->
	DB = pourse_db_adapter:new(),
	DB:delete(?BOURSE_STATUS_BUCKET, Id).



get_status_list(Status, Token, StatusCode) ->
	Url = "/bourse_status/?start_time=1&end_time=100&max_result=5",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".data.1", [
				{<<"id">>, 			bitstring},
				{<<"tedpix">>,		integer},
				{<<"volume">>,		integer},
				{<<"number">>,		integer},
				{<<"value">>,		integer},
				{<<"latest">>,		#datetime{}},
				{<<"timestamp">>,	integer}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),

	{ok, {_, _, Response}} = R,
	Json = pourse_json_helper:from_json(Response, map),
	StatusList = maps:get(<<"data">>, Json),
	Q = lists:member(Status, StatusList),
	?assertEqual(true, Q).


get_last_status(Tedpix, Volume, Number, Value, Latest, Timestamp, Token, StatusCode) ->
	Url = "/bourse_status/" ++ "last",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".status", [
				{<<"id">>, 			bitstring},
				{<<"tedpix">>,		integer},
				{<<"volume">>,		integer},
				{<<"number">>,		integer},
				{<<"value">>,		integer},
				{<<"latest">>,		#datetime{}},
				{<<"timestamp">>,	integer}
			]},

			{json, ".status", [
				{<<"id">>, 			<<"last">>},
				{<<"tedpix">>,		Tedpix},
				{<<"volume">>,		Volume},
				{<<"number">>,		Number},
				{<<"value">>,		Value},
				{<<"latest">>,		Latest},
				{<<"timestamp">>,	Timestamp}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).



get_info1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	UID = signup(Username, Password, Email, 201),
	Token = login(Username, Password, OneSignalId, 200),

	Time = utc(),

	Id = randstr(10),
	Tedpix = randstr(10),
	Volume = randstr(10),
	Number = randstr(10),
	Value = randstr(10),
	Latest = Time,
	Timestamp = 60,


	StatusMap = #{
		<<"id">>		=> Id,
		<<"tedpix">>	=> Tedpix,
		<<"volume">>	=> Volume,
		<<"number">>	=> Number,
		<<"value">>		=> Value,
		<<"latest">>	=> #{
			<<"year">>		=> Time#datetime.year,
			<<"month">>		=> Time#datetime.month,
			<<"day">>		=> Time#datetime.day,
			<<"hour">>		=> Time#datetime.hour,
			<<"minute">>	=> Time#datetime.minute
		},
		<<"timestamp">>	=> 60
	},

	Status = create_status(Id, Tedpix, Volume, Number, Value, Latest, Timestamp),
	get_status_list(StatusMap, Token, 200),
	update_last_status(Status),
	get_last_status(Tedpix, Volume, Number, Value, Latest, Timestamp, Token, 200),
	delete_status(Id),
	delete_status(<<"last">>),
	delete_user(UID).



randstr(Size) ->
	list_to_binary(string:left(uuid:to_string(uuid:uuid4()), Size)).
