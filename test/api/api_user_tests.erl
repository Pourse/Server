-module(api_user_tests).

-include_lib("eunit/include/eunit.hrl").

-export([
	signup/4,
	login/4,
	delete_user/1
]).

-include("../api_tests_config.hrl").

-include("data.hrl").


api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun signup1/0,
			fun login1/0,
			fun logout1/0
		]
	}.


setup() ->
	application:load(pourse),
	pourse_db_adapter:ensure_all_started(),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



signup(Username, Password, Email, StatusCode) ->
	Body = #{
		username => Username,
		password => Password,
		email => Email
	},
	R = efrisby:post(
		"/user/signup",
		Body,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>}
		],
		?API_CONFIG([])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	proplists:get_value(<<"id">>, Json).


login(Username, Password, OneSignalId, StatusCode) ->
	Body = #{
		username => Username,
		password => Password,
		onesignal_id => OneSignalId
	},
	R = efrisby:post(
		"/user/login",
		Body,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},
			{json_types, [
				{<<"token">>, bitstring}
			]}
		],
		?API_CONFIG([])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	proplists:get_value(<<"token">>, Json).


logout(Token, StatusCode) ->
	?assertMatch({ok, _}, efrisby:post(
		"/user/logout",
		<<"">>,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	)).


delete_user(Id) ->
	DB = pourse_db_adapter:new(),
	DB:delete(?USER_BUCKET, Id).



signup1() ->
	Username = randstr(20),
	Password = randstr(20),
	Email = null,
	Id = signup(Username, Password, Email, 201),
	undefined = signup(Username, Password, Email, 409),
	delete_user(Id).


login1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	Id = signup(Username, Password, Email, 201),
	login(Username, Password, OneSignalId, 200),
	delete_user(Id).


logout1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	Id = signup(Username, Password, Email, 201),
	Token = login(Username, Password, OneSignalId, 200),
	logout(Token, 200),
	logout(Token, 401),
	delete_user(Id).



randstr(Size) ->
	list_to_binary(string:left(uuid:to_string(uuid:uuid4()), Size)).
