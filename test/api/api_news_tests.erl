-module(api_news_tests).

-import(api_user_tests, [
	signup/4,
	login/4,
	delete_user/1
]).

-import(pourse_datetime_utils, [
	utc/0,
	timestamp/1
]).

-include_lib("eunit/include/eunit.hrl").

-include("../api_tests_config.hrl").

-include("authenticator.hrl").
-include("validator.hrl").
-include("data.hrl").
-include("api/api.hrl").
-include("api/info/news_info.hrl").
-include("data/news_data.hrl").



api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun get_info1/0
		]
	}.


setup() ->
	application:load(pourse),
	pourse_db_adapter:ensure_all_started(),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



create_news(Id, Title, Source, Text, Time, EComment, IComment, Images) ->
	DB = pourse_db_adapter:new(),
	NewNews = #news_data{
		id					= Id,
		title				= Title,
		source				= Source,
		text				= Text,
		time				= Time,
		timestamp			= 70,
		external_comments	= [EComment],
		internal_comments	= [IComment],
		images				= Images
	},
	DB:put(?NEWS_BUCKET, Id, NewNews, "timestamp", NewNews#news_data.timestamp).


add_comment(Id, Comment, Token, StatusCode) ->
	Body = #{
		text => Comment
	},
	R = efrisby:post(
		"/news/" ++ binary_to_list(Id) ++ "/internal_comments",
		Body,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	proplists:get_value(<<"user_id">>, Json).


delete_news(Id) ->
	DB = pourse_db_adapter:new(),
	DB:delete(?NEWS_BUCKET, Id).



get_news(Id, Title, Source, Text, Time, Images, Token, StatusCode) ->
	Url = "/news/" ++ binary_to_list(Id),

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, [
				{<<"title">>, bitstring},
				{<<"source">>, bitstring},
				{<<"text">>, bitstring},
				{<<"time">>, #datetime{}},
				{<<"images">>, list}
			]},

			{json, [
				{<<"title">>, Title},
				{<<"source">>, Source},
				{<<"text">>, Text},
				{<<"time">>, Time},
				{<<"images">>, Images}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).


get_news_list(News, Token, StatusCode) ->
	Url = "/news/?start_time=1&end_time=100&max_result=5",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".news.1", [
				{<<"id">>, bitstring},
				{<<"title">>, bitstring},
				{<<"time">>, #datetime{}},
				{<<"timestamp">>, integer}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),

	{ok, {_, _, Response}} = R,
	Json = pourse_json_helper:from_json(Response, map),
	NewsList = maps:get(<<"news">>, Json),
	Q = lists:member(News, NewsList),
	?assertEqual(true, Q).


get_external_comments_info(Id, EComment, Token, StatusCode) ->
	Url = "/news/" ++ binary_to_list(Id) ++ "/external_comments",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".comments.1", [
				{<<"source">>, bitstring},
				{<<"text">>, bitstring},
				{<<"time">>, #datetime{}}
			]},

			{json, ".comments.1", [
				{<<"source">>, EComment#external_comment.source},
				{<<"text">>, EComment#external_comment.text},
				{<<"time">>, EComment#external_comment.time}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).


get_internal_comments_info(Id, IComment, Token, StatusCode) ->
	Url = "/news/" ++ binary_to_list(Id) ++ "/internal_comments",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".comments.1", [
				{<<"user_id">>, bitstring},
				{<<"text">>, bitstring},
				{<<"time">>, #datetime{}}
			]},

			{json, ".comments.1", [
				{<<"user_id">>, IComment#internal_comment.user_id},
				{<<"text">>, IComment#internal_comment.text},
				{<<"time">>, IComment#internal_comment.time}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).



get_info1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	UID = signup(Username, Password, Email, 201),
	Token = login(Username, Password, OneSignalId, 200),

	Id = randstr(10),
	Title = randstr(10),
	Source = randstr(10),
	Text = randstr(10),


	Time = utc(),

	IComment = #internal_comment{
		user_id = randstr(10),
		text = randstr(10),
		time = Time
	},

	EComment = #external_comment{
		source = randstr(10),
		text = randstr(10),
		time = Time
	},

	Images = [
		<<"url1">>,
		<<"url2">>
	],

	News = #{
		<<"id">> => Id,
		<<"title">> => Title,
		<<"time">> => #{
			<<"year">> => Time#datetime.year,
			<<"month">> => Time#datetime.month,
			<<"day">> => Time#datetime.day,
			<<"hour">> => Time#datetime.hour,
			<<"minute">> => Time#datetime.minute
		},
		<<"timestamp">> => 70
	},

	Comment = randstr(10),
	create_news(Id, Title, Source, Text, Time, EComment, IComment, Images) ,
	get_news(Id, Title, Source, Text, Time, Images, Token, 200),
	get_news_list(News, Token, 200),
	get_external_comments_info(Id, EComment, Token, 200),
	get_internal_comments_info(Id, IComment, Token, 200),
	add_comment(Id, Comment, Token, 200),
	delete_news(Id),
	delete_user(UID).



randstr(Size) ->
	list_to_binary(string:left(uuid:to_string(uuid:uuid4()), Size)).
