-module(api_notification_tests).

-export([
	create_stock/7,
	delete_stock/1
]).

-import(api_user_tests, [
	signup/4,
	login/4,
	delete_user/1
]).

-import(pourse_datetime_utils, [
	utc/0
]).


-import(pourse_notification_utils, [
	add_notification/2,
	delete_notification/2
]).


-include_lib("eunit/include/eunit.hrl").

-include("../api_tests_config.hrl").

-include("authenticator.hrl").
-include("validator.hrl").
-include("data.hrl").
-include("api/api.hrl").
-include("api/info/stock_info.hrl").
-include("data/stock_data.hrl").
-include("data/notification_data.hrl").



api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun get_info1/0
		]
	}.


setup() ->
	application:load(pourse),
	pourse_db_adapter:ensure_all_started(),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



create_stock(Id, Ic, Name, Symbol, General, EComment, IComment) ->
	DB = pourse_db_adapter:new(),
	NewStock = #stock_data{
		id					= Id,
		ic					= Ic,
		name				= Name,
		symbol				= Symbol,
		news				= [],
		general_data		= [General],
		external_comments	= [EComment],
		internal_comments	= [IComment]
	},
	DB:put(?STOCK_BUCKET, Id, NewStock).


create_notif(Id) ->
	add_notification(stock, Id).


delete_stock(Id) ->
	DB = pourse_db_adapter:new(),
	DB:delete(?STOCK_BUCKET, Id).


delete_notif(Id) ->
	delete_notification(stock, Id).



enable_notification(Id, Token, StatusCode) ->
	Url = "/notification/stock/" ++ binary_to_list(Id),

	R = efrisby:post(
		Url,
		<<"">>,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, [
				{<<"observables">>, list}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	[X | _] = proplists:get_value(<<"observables">>, Json),
	?assertMatch(Id, X).


get_my_enabled_notifications(Id, Token, StatusCode) ->
	Url = "/notification/stock/",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, [
				{<<"observables">>, list}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	[X | _] = proplists:get_value(<<"observables">>, Json),
	?assertMatch(Id, X).


disable_notification(Id, Token, StatusCode) ->
	Url = "/notification/stock/" ++ binary_to_list(Id),

	R = efrisby:delete(
		Url,
		<<"">>,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, [
				{<<"observables">>, list}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	X = proplists:get_value(<<"observables">>, Json),
	?assertMatch([], X).



get_info1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	UID = signup(Username, Password, Email, 201),
	Token = login(Username, Password, OneSignalId, 200),

	Id = randstr(10),
	Ic = randstr(10),
	Name = randstr(10),
	Symbol = randstr(10),


	Time = utc(),

	IComment = #internal_comment{
		user_id = randstr(10),
		text = randstr(10),
		time = Time
	},

	EComment = #external_comment{
		source = randstr(10),
		text = randstr(10),
		time = Time
	},

	General = #stock_general_data{
		closing_price			= rand:uniform(10),
		closing_price_change	= rand:uniform(10),
		price_range_min			= rand:uniform(10),
		price_range_max			= rand:uniform(10),
		first_price				= rand:uniform(10),
		last_day_price			= rand:uniform(10),
		number_of_trades		= rand:uniform(10),
		volume_of_trades		= rand:uniform(10),
		value_of_trades			= rand:uniform(10),
		number_of_shares		= rand:uniform(10),
		last_trade_price		= rand:uniform(10),
		last_trade_price_change	= rand:uniform(10),
		last_trade_time			= Time
	},

	create_stock(Id, Ic, Name, Symbol, [General], EComment, IComment),
	create_notif(Id),
	enable_notification(Id, Token, 200),
	get_my_enabled_notifications(Id, Token, 200),
	disable_notification(Id, Token, 200),
	delete_notif(Id),
	delete_stock(Id),
	delete_user(UID).



randstr(Size) ->
	list_to_binary(string:left(uuid:to_string(uuid:uuid4()), Size)).
