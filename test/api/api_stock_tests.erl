-module(api_stock_tests).

-export([
	create_stock/7,
	delete_stock/1
]).

-import(api_user_tests, [
	signup/4,
	login/4,
	delete_user/1
]).

-import(pourse_datetime_utils, [
	utc/0,
	to_datetime/1
]).


-include_lib("eunit/include/eunit.hrl").

-include("../api_tests_config.hrl").

-include("authenticator.hrl").
-include("validator.hrl").
-include("data.hrl").
-include("api/api.hrl").
-include("api/info/stock_info.hrl").
-include("data/stock_data.hrl").



api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun get_info1/0
		]
	}.


setup() ->
	application:load(pourse),
	pourse_db_adapter:ensure_all_started(),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



create_stock(Id, Ic, Name, Symbol, Generals, EComment, IComment) ->
	DB = pourse_db_adapter:new(),
	NewStock = #stock_data{
		id					= Id,
		ic					= Ic,
		name				= Name,
		symbol				= Symbol,
		news				= [],
		general_data		= Generals,
		external_comments	= [EComment],
		internal_comments	= [IComment]
	},
	DB:put(?STOCK_BUCKET, Id, NewStock).


add_comment(Id, Comment, Token, StatusCode) ->
	Body = #{
		text => Comment
	},
	R = efrisby:post(
		"/stock/" ++ binary_to_list(Id) ++ "/internal_comments",
		Body,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	proplists:get_value(<<"user_id">>, Json).


delete_stock(Id) ->
	DB = pourse_db_adapter:new(),
	DB:delete(?STOCK_BUCKET, Id).



get_last_general_info(Id, General, Token, StatusCode) ->
	Url = "/stock/" ++ binary_to_list(Id) ++ "/last",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".general_data", [
				{<<"closing_price">>, integer},
				{<<"closing_price_change">>, integer},
				{<<"price_range_min">>, integer},
				{<<"price_range_max">>, integer},
				{<<"first_price">>, integer},
				{<<"last_day_price">>, integer},
				{<<"number_of_trades">>, integer},
				{<<"volume_of_trades">>, integer},
				{<<"value_of_trades">>, integer},
				{<<"number_of_shares">>, integer},
				{<<"last_trade_price">>, integer},
				{<<"last_trade_price_change">>, integer},
				{<<"last_trade_time">>, #datetime{}}
			]},

			{json, ".general_data", [
				{<<"closing_price">>, General#stock_general_data.closing_price},
				{<<"closing_price_change">>, General#stock_general_data.closing_price_change},
				{<<"price_range_min">>, General#stock_general_data.price_range_min},
				{<<"price_range_max">>, General#stock_general_data.price_range_max},
				{<<"first_price">>, General#stock_general_data.first_price},
				{<<"last_day_price">>, General#stock_general_data.last_day_price},
				{<<"number_of_trades">>, General#stock_general_data.number_of_trades},
				{<<"volume_of_trades">>, General#stock_general_data.volume_of_trades},
				{<<"value_of_trades">>, General#stock_general_data.value_of_trades},
				{<<"number_of_shares">>, General#stock_general_data.number_of_shares},
				{<<"last_trade_price">>, General#stock_general_data.last_trade_price},
				{<<"last_trade_price_change">>, General#stock_general_data.last_trade_price_change},
				{<<"time">>, General#stock_general_data.last_trade_time}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).


get_all_general_info(Id, Token, StatusCode) ->
	Url = "/stock/" ++ binary_to_list(Id) ++ "/?start_time=29416980000&end_time=31355700000",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, [
				{<<"general_data">>, list}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),
	{ok, Response} = R,
	Json = efrisby_resp:json(Response),
	X = proplists:get_value(<<"general_data">>, Json),
	?assertMatch(2, erlang:length(X)).


get_external_comments_info(Id, EComment, Token, StatusCode) ->
	Url = "/stock/" ++ binary_to_list(Id) ++ "/external_comments",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".comments.1", [
				{<<"source">>, bitstring},
				{<<"text">>, bitstring},
				{<<"time">>, #datetime{}}
			]},

			{json, ".comments.1", [
				{<<"source">>, EComment#external_comment.source},
				{<<"text">>, EComment#external_comment.text},
				{<<"time">>, EComment#external_comment.time}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).


get_internal_comments_info(Id, IComment, Token, StatusCode) ->
	Url = "/stock/" ++ binary_to_list(Id) ++ "/internal_comments",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			{json_types, ".comments.1", [
				{<<"user_id">>, bitstring},
				{<<"text">>, bitstring},
				{<<"time">>, #datetime{}}
			]},

			{json, ".comments.1", [
				{<<"user_id">>, IComment#internal_comment.user_id},
				{<<"text">>, IComment#internal_comment.text},
				{<<"time">>, IComment#internal_comment.time}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R).



get_info1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	UID = signup(Username, Password, Email, 201),
	Token = login(Username, Password, OneSignalId, 200),

	Id = randstr(10),
	Ic = randstr(10),
	Name = randstr(10),
	Symbol = randstr(10),


	Time = utc(),

	IComment = #internal_comment{
		user_id = randstr(10),
		text = randstr(10),
		time = Time
	},

	EComment = #external_comment{
		source = randstr(10),
		text = randstr(10),
		time = Time
	},

	General = #stock_general_data{
		closing_price			= rand:uniform(10),
		closing_price_change	= rand:uniform(10),
		price_range_min			= rand:uniform(10),
		price_range_max			= rand:uniform(10),
		first_price				= rand:uniform(10),
		last_day_price			= rand:uniform(10),
		number_of_trades		= rand:uniform(10),
		volume_of_trades		= rand:uniform(10),
		value_of_trades			= rand:uniform(10),
		number_of_shares		= rand:uniform(10),
		last_trade_price		= rand:uniform(10),
		last_trade_price_change	= rand:uniform(10),
		last_trade_time			= to_datetime({1970, 12, 30, 22, 55})
	},

	General1 = #stock_general_data{
		closing_price			= rand:uniform(10),
		closing_price_change	= rand:uniform(10),
		price_range_min			= rand:uniform(10),
		price_range_max			= rand:uniform(10),
		first_price				= rand:uniform(10),
		last_day_price			= rand:uniform(10),
		number_of_trades		= rand:uniform(10),
		volume_of_trades		= rand:uniform(10),
		value_of_trades			= rand:uniform(10),
		number_of_shares		= rand:uniform(10),
		last_trade_price		= rand:uniform(10),
		last_trade_price_change	= rand:uniform(10),
		last_trade_time			= to_datetime({1970, 12, 29, 20, 55})
	},

	General2 = #stock_general_data{
		closing_price			= rand:uniform(10),
		closing_price_change	= rand:uniform(10),
		price_range_min			= rand:uniform(10),
		price_range_max			= rand:uniform(10),
		first_price				= rand:uniform(10),
		last_day_price			= rand:uniform(10),
		number_of_trades		= rand:uniform(10),
		volume_of_trades		= rand:uniform(10),
		value_of_trades			= rand:uniform(10),
		number_of_shares		= rand:uniform(10),
		last_trade_price		= rand:uniform(10),
		last_trade_price_change	= rand:uniform(10),
		last_trade_time			= to_datetime({1970, 12, 7, 11, 23})
	},

	Comment = randstr(10),

	create_stock(Id, Ic, Name, Symbol, [General, General1, General2], EComment, IComment),
	get_last_general_info(Id, General, Token, 200),
	get_all_general_info(Id, Token, 200),
	get_external_comments_info(Id, EComment, Token, 200),
	get_internal_comments_info(Id, IComment, Token, 200),
	add_comment(Id, Comment, Token, 200),
	delete_stock(Id),
	delete_user(UID).



randstr(Size) ->
	list_to_binary(string:left(uuid:to_string(uuid:uuid4()), Size)).
