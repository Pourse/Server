-module(api_category_tests).

-import(api_user_tests,[
	signup/4,
	login/4,
	delete_user/1
]).

-import(api_stock_tests,[
	create_stock/7,
	delete_stock/1
]).

-include_lib("eunit/include/eunit.hrl").

-include("../api_tests_config.hrl").
-include("authenticator.hrl").
-include("validator.hrl").
-include("data.hrl").
-include("api/api.hrl").
-include("api/info/stock_info.hrl").
-include("data/stock_data.hrl").
-include("api/info/category_info.hrl").
-include("data/category_data.hrl").


api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun get_all_categories1/0
		]
	}.


setup() ->
	application:load(pourse),
	pourse_db_adapter:ensure_all_started(),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



create_category(IndustryName, StockIds) ->
	DB = pourse_db_adapter:new(),
	DB:put(?CATEGORY_BUCKET, IndustryName, #category_data{industry_name=IndustryName, stocks=StockIds}).


delete_category(Id) ->
	DB = pourse_db_adapter:new(),
	DB:delete(?CATEGORY_BUCKET, Id).



get_all_categories(Token, Category, StatusCode) ->
	Url = "/category/",

	R = efrisby:get(
		Url,
		[
			{status, StatusCode},
			{content_type, <<"application/json">>},

			
			{json_types, ".categories.1", [
				{<<"stocks">>, list},
				{<<"industry_name">>, bitstring}
			]},

			{json_types, ".categories.1.stocks.1", [
				{<<"id">>, bitstring},
				{<<"name">>, bitstring},
				{<<"symbol">>, bitstring},
				{<<"ic">>, bitstring}
			]}
		],
		?API_CONFIG([
			{"Token", Token}
		])
	),
	?assertMatch({ok, _}, R),

	{ok, {_, _, Response}} = R,
	Json = pourse_json_helper:from_json(Response, map),
	CategoryList = maps:get(<<"categories">>, Json),
	Q = lists:member(Category, CategoryList),
	?assertEqual(true, Q).



get_all_categories1() ->
	Username = randstr(20),
	Password = randstr(20),
	OneSignalId = list_to_binary(uuid:to_string(uuid:uuid4())),
	Email = null,
	UId = signup(Username, Password, Email, 201),
	Token = login(Username, Password, OneSignalId, 200),

	SId1 = randstr(10),
	SId2 = randstr(10),
	SIc1 = randstr(10),
	SIc2 = randstr(10),
	SName1 = randstr(10),
	SName2 = randstr(10),
	SSymbol1 = randstr(10),
	SSymbol2 = randstr(10),

	IndustryName = randstr(10),
	StockIds = [SId1, SId2],
	StockInfos = [
		#{
			<<"id">> => SId1,
			<<"ic">> => SIc1,
			<<"name">> => SName1,
			<<"symbol">> => SSymbol1
		},
		#{
			<<"id">> => SId2,
			<<"ic">> => SIc2,
			<<"name">> => SName2,
			<<"symbol">> => SSymbol2
		}
	],
	Category = #{
		<<"industry_name">> => IndustryName,
		<<"stocks">> => StockInfos
	},

	create_stock(SId1, SIc1, SName1, SSymbol1, [], null, null),
	create_stock(SId2, SIc2, SName2, SSymbol2, [], null, null),
	create_category(IndustryName, StockIds),

	get_all_categories(Token, Category, 200),
	delete_stock(SId1),
	delete_stock(SId2),
	delete_category(IndustryName),
	delete_user(UId).


randstr(Size) ->
	list_to_binary(string:left(uuid:to_string(uuid:uuid4()), Size)).
