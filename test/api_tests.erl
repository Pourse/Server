-module(api_tests).

-include_lib("eunit/include/eunit.hrl").

-include("api_tests_config.hrl").


api_test_() ->
	{setup,
		fun setup/0,
		fun teardown/1,
		[
			fun bad_url_get/0,
			fun bad_url_post/0
		]
	}.


setup() ->
	application:load(pourse),
	application:ensure_all_started(efrisby).


teardown(_) ->
	application:stop(efrisby).



bad_url_get() ->
	?assertMatch({ok, _}, efrisby:get(
		"/bad_url",
		[
			{status, 404},
			{content_type, <<"application/json">>}
		],
		?API_CONFIG([])
	)).


bad_url_post() ->
	?assertMatch({ok, _}, efrisby:post(
		"/bad_url",
		<<"">>,
		[
			{status, 404},
			{content_type, <<"application/json">>}
		],
		?API_CONFIG([])
	)).
