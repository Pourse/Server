-ifndef(pourse_auth).
-define(pourse_auth, true).


-define(authenticate, pourse_auth:authenticate()).
-define(authorize, pourse_auth:authorize()).


-endif. % pourse_auth
