-ifndef(pourse_config).
-define(pourse_config, true).


-define(env, pourse:get_env()).
-define(env(Tag), pourse:get_env(Tag)).
-define(env(Tag, Key), pourse:get_env(Tag, Key)).


-endif. % pourse_config
