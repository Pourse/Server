-ifndef(pourse_api_internal_comment_info).
-define(pourse_api_internal_comment_info, true).

-include("data/internal_comment_data.hrl").


-record(internal_comment_info, {
	comments		:: [#internal_comment_data{}],
	continuation	:: string()
}).


-export_record_info([
	internal_comment_info
]).


-endif. % pourse_api_internal_comment_info
