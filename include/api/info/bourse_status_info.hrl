-ifndef(pourse_api_bourse_status_info).
-define(pourse_api_bourse_status_info, true).

-include("data/bourse_status_data.hrl").


-record(bourse_status_info, {
	status				:: #bourse_status_data{}
}).


-record(bourse_status_list_info, {
	status				:: [#bourse_status_data{}],
	continuation		:: string()
}).


-export_record_info([
	bourse_status_info,
	bourse_status_list_info
]).


-endif. % pourse_api_bourse_status_info
