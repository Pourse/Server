-ifndef(pourse_api_user_info).
-define(pourse_api_user_info, true).


-record(user_info, {
	id 				:: string(),
	username		:: string(),
	email			:: string()
}).


-export_record_info([
	user_info
]).


-endif. % pourse_api_user_info
