-ifndef(pourse_api_notification_info).
-define(pourse_api_notification_info, true).


-record(enabled_notifications_info, {
	observables		:: [string()]
}).


-export_record_info([
	enabled_notifications_info
]).


-endif. % pourse_api_notification_info
