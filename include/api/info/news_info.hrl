-ifndef(pourse_api_news_info).
-define(pourse_api_news_info, true).

-include("data/news_data.hrl").


-record(news_general_info, {
	news					:: #news_data{}
}).


-record(news_basic_info, {
	id					:: string(),
	title				:: string(),
	time				:: integer(),
	image				:: string()
}).

-record(news_list_info, {
	news					:: [#news_basic_info{}],
	continuation			:: string()
}).


-export_record_info([
	news_general_info,
	news_basic_info,
	news_list_info
]).


-endif. % pourse_api_news_info
