-ifndef(pourse_api_category_info).
-define(pourse_api_category_info, true).

-include("data/category_data.hrl").


-record(category_info, {
	categories			:: [#category_item{}]
}).


-export_record_info([
	category_info
]).

-endif. % pourse_api_category_info
