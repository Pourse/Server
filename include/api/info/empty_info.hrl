-ifndef(pourse_api_empty_info).
-define(pourse_api_empty_info, true).


-record(empty_info, {
}).


-export_record_info([
	empty_info
]).


-endif. % pourse_api_empty_info
