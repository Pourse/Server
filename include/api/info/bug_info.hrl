-ifndef(pourse_api_bug_info).
-define(pourse_api_bug_info, true).

-include("data/bug_data.hrl").
-include("api/info/user_info.hrl").


-record(bug_info, {
	id					:: string(),
	user				:: #user_info{},
	details				:: string(),
	time				:: integer(),
	checked				:: boolean(),
	title				:: string()
}).

-record(bug_info_list, {
	bugs			:: [#bug_info{}],
	continuation	:: string()
}).


-export_record_info([
	bug_info,
	bug_info_list
]).


-endif. % pourse_api_bug_info
