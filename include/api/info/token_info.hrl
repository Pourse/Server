-ifndef(pourse_api_token_info).
-define(pourse_api_token_info, true).


-record(token_info, {
	token 			:: string()
}).


-export_record_info([
	token_info
]).


-endif. % pourse_api_token_info
