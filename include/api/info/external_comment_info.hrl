-ifndef(pourse_api_external_comment_info).
-define(pourse_api_external_comment_info, true).

-include("data/external_comment_data.hrl").


-record(external_comment_info, {
	comments		:: [#external_comment_data{}],
	continuation	:: string()
}).


-export_record_info([
	external_comment_info
]).


-endif. % pourse_api_external_comment_info
