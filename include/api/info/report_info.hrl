-ifndef(pourse_api_report_info).
-define(pourse_api_report_info, true).

-include("data/report_data.hrl").
-include("data/internal_comment_data.hrl").


-record(reports_info, {
	reports			:: [#report_data{}],
	comments		:: [#internal_comment_data{}],
	continuation	:: string()
}).


-export_record_info([
	reports_info
]).


-endif. % pourse_api_report_info
