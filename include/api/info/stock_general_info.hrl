-ifndef(pourse_api_stock_general_info).
-define(pourse_api_stock_general_info, true).

-include("data/stock_data.hrl").


-record(stock_general_info, {
	general_data				:: [#stock_general_data{}],
	continuation				:: string()
}).

-record(stock_last_general_info, {
	general_data				:: #stock_general_data{}
}).


-export_record_info([
	stock_general_info,
	stock_last_general_info
]).


-endif. % pourse_api_stock_general_info
