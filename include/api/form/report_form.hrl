-ifndef(pourse_api_report_form).
-define(pourse_api_report_form, true).


-record(report_form, {
	owner_id			:: string(),
	comment_id			:: string(),
	reason				:: string()
}).


-export_record_info([
	report_form
]).


-endif. % pourse_api_report_form