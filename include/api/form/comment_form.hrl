-ifndef(pourse_api_comment_form).
-define(pourse_api_comment_form, true).


-record(comment_form, {
	text				:: string()
}).


-export_record_info([
	comment_form
]).


-endif. % pourse_api_comment_form