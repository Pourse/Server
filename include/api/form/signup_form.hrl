-ifndef(pourse_api_signup_form).
-define(pourse_api_signup_form, true).


-record(signup_form, {
	username 			:: string(),
	email 				:: string(),
	password 			:: string()
}).


-export_record_info([
	signup_form
]).


-endif. % pourse_api_signup_form
