-ifndef(pourse_api_login_form).
-define(pourse_api_login_form, true).


-record(login_form, {
	username 			:: string(),
	password 			:: string(),
	onesignal_id		:: string()
}).


-export_record_info([
	login_form
]).


-endif. % pourse_api_login_form
