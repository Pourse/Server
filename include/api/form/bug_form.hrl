-ifndef(pourse_api_bug_form).
-define(pourse_api_bug_form, true).


-record(bug_form, {
	details			:: string(),
	title			:: string()
}).


-export_record_info([
	bug_form
]).


-endif. % pourse_api_bug_form