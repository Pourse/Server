-ifndef(pourse_api_controller).
-define(pourse_api_controller, true).


-define(request, pourse_api_controller:get_request()).
-define(request(Attr), pourse_api_controller:get_request(Attr)).
-define(request(Attr, Param), pourse_api_controller:get_request(Attr, Param)).

-define(abort(StatusCode), pourse_api_controller:abort(StatusCode)).
-define(abort(StatusCode, Reason), pourse_api_controller:abort(StatusCode, Reason)).


-endif. % pourse_api_controller
