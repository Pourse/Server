-ifndef(pourse_data_bug).
-define(pourse_data_bug, true).


-record(bug_data, {
	id					:: binary(),
	user_id				:: binary(),
	details				:: binary(),
	time				:: integer(),
	checked				:: boolean(),
	title				:: string()
}).


-endif. % pourse_data_bug
