-ifndef(pourse_data_stock).
-define(pourse_data_stock, true).


-record(stock_general_data, {
	id							:: binary(),
	closing_price				:: integer(),
	closing_price_change		:: number(),
	price_range_min				:: integer(),
	price_range_max				:: integer(),
	first_price					:: integer(),
	last_day_price				:: integer(),
	number_of_trades			:: integer(),
	volume_of_trades			:: integer(),
	value_of_trades				:: integer(),
	number_of_shares			:: integer(),
	last_trade_price			:: integer(),
	last_trade_price_change		:: number(),
	last_trade_time				:: integer()
}).


-record(stock_data, {
	id							:: binary(),
	name						:: binary(),
	symbol						:: binary(),
	ic							:: binary()
}).


-record(stock_basic_data, {
	id							:: binary(),
	name						:: binary(),
	symbol						:: binary()
}).


-export_record_info([
	stock_general_data,
	stock_basic_data
]).


-endif. % pourse_data_stock
