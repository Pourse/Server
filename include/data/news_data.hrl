-ifndef(pourse_data_news).
-define(pourse_data_news, true).


-record(news_data, {
	id							:: binary(),
	title						:: binary(),
	source						:: binary(),
	text						:: binary(),
	time						:: integer(),
	images						:: [binary()]
}).

-export_record_info([
	news_data
]).


-endif. % pourse_data_news
