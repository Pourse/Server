-ifndef(pourse_data_external_comment).
-define(pourse_data_external_comment, true).


-record(external_comment_data, {
	id							:: binary(),
	source						:: binary(),
	text						:: binary(),
	time						:: integer()
}).


-export_record_info([
	external_comment_data
]).


-endif. % pourse_data_external_comment
