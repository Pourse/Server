-ifndef(pourse_data_internal_comment).
-define(pourse_data_internal_comment, true).


-record(internal_comment_data, {
	id							:: binary(),
	user_id						:: binary(),
	username					:: binary(),
	text						:: binary(),
	time						:: integer(),
	deleted						:: boolean(),
	approved					:: boolean()
}).


-export_record_info([
	internal_comment_data
]).


-endif. % pourse_data_internal_comment
