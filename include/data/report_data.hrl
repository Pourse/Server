-ifndef(pourse_data_report).
-define(pourse_data_report, true).


-record(report_data, {
	id					:: binary(),
	owner_id			:: binary(),
	comment_id			:: binary(),
	user_id				:: binary(),
	reason				:: binary(),
	time				:: integer(),
	checked				:: boolean()
}).


-export_record_info([
	report_data
]).


-endif. % pourse_data_report
