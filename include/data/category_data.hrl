-ifndef(pourse_data_category).
-define(pourse_data_category, true).

-include("data/stock_data.hrl").


-record(category_item, {
	name				:: binary(),
	stocks				:: [#stock_basic_data{}]
}).

-record(category_data, {
	type				:: binary(),
	items				:: [#category_item{}]
}).


-export_record_info([
	category_item
]).


-endif. % pourse_data_category
