-ifndef(pourse_data_user).
-define(pourse_data_user, true).


-record(user_data, {
	id 				:: binary(),
	username		:: binary(),
	email			:: binary(),
	password		:: binary(),
	observables		:: [binary()],
	onesignal_ids	:: [binary()],
	max_reports		:: integer(),
	report_count	:: integer(),
	last_report_day	:: integer()
}).


-endif. % pourse_data_user
