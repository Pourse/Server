-ifndef(pourse_data_bourse_status).
-define(pourse_data_bourse_status, true).


-record(bourse_status_data, {
	id							:: binary(),
	tedpix						:: number(),
	volume						:: integer(),
	number						:: integer(),
	value						:: integer(),
	time						:: integer()
}).

-export_record_info([
	bourse_status_data
]).


-endif. % pourse_data_bourse_status
