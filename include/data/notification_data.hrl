-ifndef(pourse_data_notification).
-define(pourse_data_notification, true).


-record(notification_data, {
	id							:: binary(),
	observers					:: [binary()]
}).


-endif. % pourse_data_notification
