-ifndef(pourse_data).
-define(pourse_data, true).

-include("config.hrl").


-define(USER_BUCKET, ?env(data, user_bucket)).
-define(CATEGORY_BUCKET, ?env(data, category_bucket)).
-define(STOCK_BUCKET, ?env(data, stock_bucket)).
-define(NEWS_BUCKET, ?env(data, news_bucket)).
-define(BOURSE_STATUS_BUCKET, ?env(data, bourse_status_bucket)).
-define(NOTIFICATION_BUCKET, ?env(data, notification_bucket)).

-define(STOCK_GENERAL_DATA_BUCKET(StockId), 
	<< (?env(data, stock_general_data_bucket))/binary, "_", StockId/binary >>
).

-define(EXTERNAL_COMMENT_BUCKET(OwnerId), 
	<< (?env(data, external_comment_bucket))/binary, "_", OwnerId/binary >>
).

-define(INTERNAL_COMMENT_BUCKET(OwnerId), 
	<< (?env(data, internal_comment_bucket))/binary, "_", OwnerId/binary >>
).

-define(REPORT_BUCKET, ?env(data, report_bucket)).

-define(BUG_BUCKET, ?env(data, bug_bucket)).


-endif. % pourse_data
