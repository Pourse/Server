-module(pourse_external_comment_controller).

-export([
	create/2,
	get/2,
	get/5
]).

-include("data.hrl").
-include("data/external_comment_data.hrl").


create(OwnerId, ExternalCommentData = #external_comment_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?EXTERNAL_COMMENT_BUCKET(OwnerId),
	Id = make_id(ExternalCommentData),
	Ts = ExternalCommentData#external_comment_data.time,
	ECD = ExternalCommentData#external_comment_data{id=Id},
	DB:put(Bucket, Id, ECD, "timestamp", -Ts),
	Id.


get(OwnerId, Id) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?EXTERNAL_COMMENT_BUCKET(OwnerId),
	ExternalCommentObj = DB:get(Bucket, Id),
	ExternalCommentObj:get_value().


get(OwnerId, StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?EXTERNAL_COMMENT_BUCKET(OwnerId),
	{ok, Result, NewContinuation} = DB:get_index(Bucket, "timestamp", -EndTime, -StartTime, MaxResult, Continuation),
	AllExternalCommentData = lists:map(
		fun(Id) ->
			{ok, Data} = get(OwnerId, Id),
			Data
		end,
		Result
	),
	{AllExternalCommentData, NewContinuation}.


make_id(Obj) ->
	Hash = erlang:phash2(Obj),
	Size = erlang:size(term_to_binary(Obj)),
	list_to_binary(integer_to_list(Hash) ++ "_" ++ integer_to_list(Size)).
