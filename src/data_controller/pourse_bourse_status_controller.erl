-module(pourse_bourse_status_controller).

-export([
	create/1,
	get/1,
	get_last/0,
	get/4
]).

-include("data.hrl").
-include("data/bourse_status_data.hrl").


create(BourseStatusData = #bourse_status_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Ts = BourseStatusData#bourse_status_data.time,
	BSD = BourseStatusData#bourse_status_data{id=Id},
	DB:put(?BOURSE_STATUS_BUCKET, Id, BSD, "timestamp", -Ts),
	DB:put(?BOURSE_STATUS_BUCKET, <<"last">>, BSD),
	Id.


get(Id) ->
	DB = pourse_db_adapter:new(),
	BourseStatusObj = DB:get(?BOURSE_STATUS_BUCKET, Id),
	BourseStatusObj:get_value().


get_last() ->
	DB = pourse_db_adapter:new(),
	BourseStatusObj = DB:get(?BOURSE_STATUS_BUCKET, <<"last">>),
	BourseStatusObj:get_value().


get(StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	{ok, Result, NewContinuation} = DB:get_index(?BOURSE_STATUS_BUCKET, "timestamp", -EndTime, -StartTime, MaxResult, Continuation),
	AllBourseStatusData = lists:map(
		fun(Id) ->
			{ok, Data} = ?MODULE:get(Id),
			Data
		end,
		Result
	),
	{AllBourseStatusData, NewContinuation}.
