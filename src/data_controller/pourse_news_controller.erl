-module(pourse_news_controller).

-export([
	create/1,
	get/2,
	get/4,
	exists/2
]).

-include("data.hrl").
-include("data/news_data.hrl").


create(NewsData = #news_data{id=undefined, images=Images}) when is_list(Images) ->
	DB = pourse_db_adapter:new(),
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Ts = NewsData#news_data.time,
	ND = NewsData#news_data{id=Id},
	Obj = DB:put(?NEWS_BUCKET, Id, ND, "timestamp", -Ts),
	Obj:add_index("title", NewsData#news_data.title),
	Id.


get(id, Id) ->
	DB = pourse_db_adapter:new(),
	NewsObj = DB:get(?NEWS_BUCKET, Id),
	NewsObj:get_value();


get(title, Title) ->
	DB = pourse_db_adapter:new(),
	case DB:get_index(?NEWS_BUCKET, "title", Title) of
		{ok, []} ->
			{error, notfound};
		{ok, [Id]} ->
			get(id, Id)
	end.


get(StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	{ok, Result, NewContinuation} = DB:get_index(?NEWS_BUCKET, "timestamp", -EndTime, -StartTime, MaxResult, Continuation),
	AllNewsData = lists:map(
		fun(Id) ->
			{ok, Data} = ?MODULE:get(id, Id),
			Data
		end,
		Result
	),
	{AllNewsData, NewContinuation}.


exists(title, Title) ->
	DB = pourse_db_adapter:new(),
	case DB:get_index(?NEWS_BUCKET, "title", Title) of
		{ok, []} ->
			false;
		{ok, [Id]} ->
			{true, Id}
	end.
