-module(pourse_bug_controller).

-export([
	create/1,
	get/1,
	get/4,
	update/1
]).

-include("data.hrl").
-include("data/bug_data.hrl").


create(BugData = #bug_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?BUG_BUCKET,
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Ts = BugData#bug_data.time,
	BD = BugData#bug_data{
		id = Id,
		checked = false
	},
	DB:put(Bucket, Id, BD, "timestamp", Ts),
	Id.


get(Id) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?BUG_BUCKET,
	BugObj = DB:get(Bucket, Id),
	BugObj:get_value().


get(StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?BUG_BUCKET,
	{ok, Result, NewContinuation} = DB:get_index(Bucket, "timestamp", StartTime, EndTime, MaxResult, Continuation),
	AllBugData = lists:filtermap(
		fun(Id) ->
			{ok, Data} = ?MODULE:get(Id),
			case Data#bug_data.checked of
				true -> false;
				false -> {true, Data}
			end
		end,
		Result
	),
	{AllBugData, NewContinuation}.


update(Data = #bug_data{id=Id}) when Id /= undefined ->
	DB = pourse_db_adapter:new(),
	BugObj = DB:get(?BUG_BUCKET, Id),
	BugObj:update(Data).
