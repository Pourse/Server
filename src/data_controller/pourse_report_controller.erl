-module(pourse_report_controller).

-export([
	create/1,
	get/1,
	get/4,
	update/1
]).

-include("data.hrl").
-include("data/report_data.hrl").


create(ReportData = #report_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?REPORT_BUCKET,
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Ts = ReportData#report_data.time,
	ICRD = ReportData#report_data{
		id = Id,
		checked = false
	},
	DB:put(Bucket, Id, ICRD, "timestamp", Ts),
	Id.


get(Id) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?REPORT_BUCKET,
	ReportObj = DB:get(Bucket, Id),
	ReportObj:get_value().


get(StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?REPORT_BUCKET,
	{ok, Result, NewContinuation} = DB:get_index(Bucket, "timestamp", StartTime, EndTime, MaxResult, Continuation),
	AllReportsData = lists:filtermap(
		fun(Id) ->
			{ok, Data} = ?MODULE:get(Id),
			case Data#report_data.checked of
				true -> false;
				false -> {true, Data}
			end
		end,
		Result
	),
	AllCommentsData = lists:map(
		fun(Rep) ->
			OwnerId = Rep#report_data.owner_id,
			CommentId = Rep#report_data.comment_id,
			{ok, Data} = pourse_internal_comment_controller:get(OwnerId, CommentId),
			Data
		end,
		AllReportsData
	),
	{AllReportsData, AllCommentsData, NewContinuation}.


update(Data = #report_data{id=Id}) when Id /= undefined ->
	DB = pourse_db_adapter:new(),
	ReportObj = DB:get(?REPORT_BUCKET, Id),
	ReportObj:update(Data).
