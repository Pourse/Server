-module(pourse_notification_controller).

-export([
	create/3,
	get/2,
	update/1
]).

-include("data.hrl").
-include("data/notification_data.hrl").


create(Tag, OwnerId, NotificationData = #notification_data{id=undefined, observers=Observers})
		when is_list(Observers) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?NOTIFICATION_BUCKET,
	Id = make_id(Tag, OwnerId),
	ND = NotificationData#notification_data{id=Id},
	DB:put(Bucket, Id, ND),
	Id.


get(Tag, OwnerId) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?NOTIFICATION_BUCKET,
	Id = make_id(Tag, OwnerId),
	NotificationObj = DB:get(Bucket, Id),
	NotificationObj:get_value().


update(Data = #notification_data{id=Id}) when Id /= undefined ->
	DB = pourse_db_adapter:new(),
	NotifObj = DB:get(?NOTIFICATION_BUCKET, Id),
	NotifObj:update(Data).


make_id(Tag, OwnerId) ->
	list_to_binary(atom_to_list(Tag) ++ "_" ++ binary_to_list(OwnerId)).
