-module(pourse_internal_comment_controller).

-export([
	create/2,
	get/2,
	get/5,
	update/2,
	delete/2
]).

-include("data.hrl").
-include("data/internal_comment_data.hrl").


create(OwnerId, InternalCommentData = #internal_comment_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?INTERNAL_COMMENT_BUCKET(OwnerId),
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Ts = InternalCommentData#internal_comment_data.time,
	ICD = InternalCommentData#internal_comment_data{
		id			= Id,
		deleted		= false,
		approved	= true
	},
	DB:put(Bucket, Id, ICD, "timestamp", -Ts),
	Id.


get(OwnerId, Id) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?INTERNAL_COMMENT_BUCKET(OwnerId),
	InternalCommentObj = DB:get(Bucket, Id),
	InternalCommentObj:get_value().


get(OwnerId, StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?INTERNAL_COMMENT_BUCKET(OwnerId),
	{ok, Result, NewContinuation} = DB:get_index(Bucket, "timestamp", -EndTime, -StartTime, MaxResult, Continuation),
	AllInternalCommentData = lists:map(
		fun(Id) ->
			{ok, Data} = get(OwnerId, Id),
			Data
		end,
		Result
	),
	{AllInternalCommentData, NewContinuation}.


update(OwnerId, Data = #internal_comment_data{id=Id}) when Id /= undefined ->
	DB = pourse_db_adapter:new(),
	Bucket = ?INTERNAL_COMMENT_BUCKET(OwnerId),
	InternalCommentObj = DB:get(Bucket, Id),
	InternalCommentObj:update(Data).


delete(OwnerId, Id) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?INTERNAL_COMMENT_BUCKET(OwnerId),
	InternalCommentObj = DB:get(Bucket, Id),
	InternalCommentObj:delete().
