-module(pourse_stock_controller).

-export([
	create/1,
	get/2,
	exists/2,
	all/0,
	add_general_data/2,
	get_general_data/2,
	get_general_data/5,
	get_last_general_data/1
]).

-include("data.hrl").
-include("data/stock_data.hrl").


create(StockData = #stock_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Symbol = StockData#stock_data.symbol,
	SBD = StockData#stock_data{id=Id},
	DB:put(?STOCK_BUCKET, Id, SBD, "symbol", Symbol),
	Id.


get(id, Id) ->
	DB = pourse_db_adapter:new(),
	StockObj = DB:get(?STOCK_BUCKET, Id),
	StockObj:get_value();


get(symbol, Symbol) ->
	DB = pourse_db_adapter:new(),
	case DB:get_index(?STOCK_BUCKET, "symbol", Symbol) of
		{ok, []} ->
			{error, notfound};
		{ok, [Id]} ->
			get(id, Id)
	end.


exists(symbol, Symbol) ->
	DB = pourse_db_adapter:new(),
	case DB:get_index(?STOCK_BUCKET, "symbol", Symbol) of
		{ok, []} ->
			false;
		{ok, [Id]} ->
			{true, Id}
	end.


all() ->
	DB = pourse_db_adapter:new(),
	DB:list_keys(?STOCK_BUCKET).


add_general_data(StockId, StockGeneralData = #stock_general_data{id=undefined}) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?STOCK_GENERAL_DATA_BUCKET(StockId),
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Ts = StockGeneralData#stock_general_data.last_trade_time,
	SGD = StockGeneralData#stock_general_data{id=Id},
	DB:put(Bucket, Id, SGD, "timestamp", -Ts),
	DB:put(Bucket, <<"last">>, SGD),
	Id.


get_general_data(StockId, Id) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?STOCK_GENERAL_DATA_BUCKET(StockId),
	StockGeneralDataObj = DB:get(Bucket, Id),
	StockGeneralDataObj:get_value().


get_general_data(StockId, StartTime, EndTime, MaxResult, Continuation) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?STOCK_GENERAL_DATA_BUCKET(StockId),
	{ok, Result, NewContinuation} = DB:get_index(Bucket, "timestamp", -EndTime, -StartTime, MaxResult, Continuation),
	AllGeneralData = lists:map(
		fun(Id) ->
			{ok, Data} = get_general_data(StockId, Id),
			Data
		end,
		Result
	),
	{AllGeneralData, NewContinuation}.


get_last_general_data(StockId) ->
	DB = pourse_db_adapter:new(),
	Bucket = ?STOCK_GENERAL_DATA_BUCKET(StockId),
	StockGeneralDataObj = DB:get(Bucket, <<"last">>),
	StockGeneralDataObj:get_value().
