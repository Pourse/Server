-module(pourse_user_controller).

-export([
	create/1,
	get/2,
	update/1
]).

-include("data.hrl").
-include("data/user_data.hrl").


create(UserData = #user_data{id=undefined, observables=Observables, onesignal_ids=OnesignalIds})
		when is_list(Observables) andalso is_list(OnesignalIds) ->
	DB = pourse_db_adapter:new(),
	Id = list_to_binary(uuid:to_string(uuid:uuid4())),
	Username = UserData#user_data.username,
	UD = UserData#user_data{
		id = Id,
		max_reports = 3,
		report_count = 0,
		last_report_day = pourse_datetime_utils:timestamp(day)
	},
	DB:put(?USER_BUCKET, Id, UD, "username", Username),
	Id.


get(id, Id) ->
	DB = pourse_db_adapter:new(),
	UserObj = DB:get(?USER_BUCKET, Id),
	UserObj:get_value();


get(username, Username) ->
	DB = pourse_db_adapter:new(),
	case DB:get_index(?USER_BUCKET, "username", Username) of
		{ok, []} ->
			{error, notfound};
		{ok, [Id]} ->
			get(id, Id)
	end.


update(Data = #user_data{id=Id}) when Id /= undefined ->
	DB = pourse_db_adapter:new(),
	UserObj = DB:get(?USER_BUCKET, Id),
	UserObj:update(Data).
