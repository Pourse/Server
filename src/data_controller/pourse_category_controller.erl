-module(pourse_category_controller).

-export([
	create/1,
	get/1
]).

-include("data.hrl").
-include("data/category_data.hrl").


create(CategoryData = #category_data{type=Type, items=Items})
		when Type /= undefined andalso is_list(Items) ->
	DB = pourse_db_adapter:new(),
	Id = erlang:atom_to_binary(Type, latin1),
	DB:put(?CATEGORY_BUCKET, Id, CategoryData),
	Id.


get(Type) ->
	DB = pourse_db_adapter:new(),
	Id = erlang:atom_to_binary(Type, latin1),
	CategoryObj = DB:get(?CATEGORY_BUCKET, Id),
	CategoryObj:get_value().
