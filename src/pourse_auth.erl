-module(pourse_auth).

-export([
	find_user/1,
	add_token/1,
	del_token/0,
	authenticate/0,
	authorize/0
]).

-include("config.hrl").
-include("api/api.hrl").
-include_lib("eunit/include/eunit.hrl").

-define(ADMINKEY, ?env(api, admin_key)).


find_user(Token) ->
	Rds = pourse_redis_adapter:new(),
	case Rds:get(Token) of
		undefined -> badtoken;
		UserId -> {ok, UserId}
	end.


add_token(UserId) ->
	Token = list_to_binary(uuid:to_string(uuid:uuid4())),
	Rds = pourse_redis_adapter:new(),
	ok = Rds:setex(Token, UserId, 3 * 30 * 24 * 3600),
	Token.


del_token() ->
	Token = ?request(headers, "Token"),
	Rds = pourse_redis_adapter:new(),
	ok = Rds:del(Token).


authenticate() ->
	Token = ?request(headers, "Token"),
	{ok, UserId} = find_user(Token),
	UserId.


authorize() ->
	Key = ?request(headers, "Admin-Key"),
	?assertEqual(?ADMINKEY, Key).
