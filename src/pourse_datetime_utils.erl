-module(pourse_datetime_utils).

-export([
	timestamp/0,
	timestamp/1
]).


timestamp() ->
	{Mega, Secs, _Micro} = erlang:timestamp(),
	Now = (Mega * 1000000 + Secs),
	Now.


timestamp(day) ->
	timestamp() div (24 * 3600).
