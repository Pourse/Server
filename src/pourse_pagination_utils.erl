-module(pourse_pagination_utils).

-export([
	pagination_args/0
]).

-include("api/api.hrl").


pagination_args() ->
	Args = ?request(args),
	StartTime = proplists:get_value("start_time", Args),
	EndTime = proplists:get_value("end_time", Args),
	MaxResult = proplists:get_value("max_result", Args, "10"),
	Continuation = proplists:get_value("continuation", Args),

	StartTimeInt = list_to_integer(StartTime),
	EndTimeInt = list_to_integer(EndTime),
	MaxResultInt = lists:min([list_to_integer(MaxResult), 15]),

	{StartTimeInt, EndTimeInt, MaxResultInt, Continuation}.
