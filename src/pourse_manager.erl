-module(pourse_manager).

-behaviour(gen_server).

-export([
	start/0,
	upgrade_code/0
]).

-export([
	update_categories/0,
	update_stocks/0,
	update_stock_comments/0,
	update_news/2,
	update_bourse_status/0
]).

-export([
	init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3
]).

-include("data.hrl").
-include("data/category_data.hrl").
-include("data/stock_data.hrl").
-include("data/external_comment_data.hrl").
-include("data/news_data.hrl").
-include("data/bourse_status_data.hrl").

-define(add_job(Job), pourse_jobqueue:add_job(scrape, Job)).
-define(add_notif_job(Job), pourse_jobqueue:add_job(notification, Job)).


start() ->
	pourse_jobqueue:add_queue(scrape),
	pourse_jobqueue:add_queue(notification),
	gen_server:start_link(?MODULE, [], []),
	ok.


upgrade_code() ->
	pass.



update_categories() ->
	?add_job(fun() -> update_categories_job() end).

update_categories_job() ->
	IndustryCategoryData = pourse_scraper:scrape_categories(),
	CategoryItems = 
	lists:map(
		fun({IndustryName, Stocks}) ->
			StocksBasicData = lists:foldl(
				fun(S, R) ->
					BasicData =
					case pourse_stock_controller:get(symbol, maps:get(<<"symbol">>, S)) of
						{ok, StockData} ->
							#stock_basic_data{
								id = StockData#stock_data.id,
								name = StockData#stock_data.name,
								symbol = StockData#stock_data.symbol
							};

						{error, notfound} ->
							StockData = #stock_data{
								name	= maps:get(<<"name">>, S),
								symbol	= maps:get(<<"symbol">>, S),
								ic 		= maps:get(<<"ic">>, S)
							},
							StockId = pourse_stock_controller:create(StockData),
							pourse_notification_utils:add_notification(stock, StockId),

							#stock_basic_data{
								id = StockId,
								name = StockData#stock_data.name,
								symbol = StockData#stock_data.symbol
							}
					end,
					[BasicData | R]
				end,
				[],
				Stocks
			),

			#category_item{
				name = IndustryName,
				stocks = StocksBasicData
			}
		end,
		maps:to_list(maps:get(<<"categories">>, IndustryCategoryData))
	),

	CategoryData = #category_data{
		type = industry,
		items = CategoryItems
	},
	pourse_category_controller:create(CategoryData),
	ok.



update_stocks() ->
	Stocks = pourse_stock_controller:all(),
	lists:foreach(
		fun(StockId) ->
			?add_job(fun() -> update_stock_job(StockId) end)
		end,
		Stocks
	),
	ok.

update_stock_job(StockId) ->
	{ok, StockData} = pourse_stock_controller:get(id, StockId),
	Ic = StockData#stock_data.ic,

	GeneralInfo = pourse_scraper:scrape_stock_general_info(Ic),
	case GeneralInfo =:= #{} of
		true -> notfound;
		false ->
			LastGeneralData = pourse_stock_controller:get_last_general_data(StockId),
			IsNew =
			case LastGeneralData of
				{error, notfound} -> true;
				{ok, Data} ->
					LastDateTime = maps:get(<<"last_trade_time">>, GeneralInfo),
					case LastDateTime > Data#stock_general_data.last_trade_time of
						true -> true;
						false -> false
					end
			end,
			case IsNew of
				false -> void;
				true ->
					NewGeneralData = #stock_general_data{
						closing_price				= maps:get(<<"closing_price">>, GeneralInfo),
						closing_price_change		= maps:get(<<"closing_price_change">>, GeneralInfo),
						price_range_min				= maps:get(<<"price_range_min">>, GeneralInfo),
						price_range_max				= maps:get(<<"price_range_max">>, GeneralInfo),
						first_price					= maps:get(<<"first_price">>, GeneralInfo),
						last_day_price				= maps:get(<<"last_day_price">>, GeneralInfo),
						number_of_trades			= maps:get(<<"number_of_trades">>, GeneralInfo),
						value_of_trades				= maps:get(<<"value_of_trades">>, GeneralInfo),
						number_of_shares			= maps:get(<<"number_of_shares">>, GeneralInfo),
						last_trade_price			= maps:get(<<"last_trade_price">>, GeneralInfo),
						last_trade_price_change		= maps:get(<<"last_trade_price_change">>, GeneralInfo),
						last_trade_time				= maps:get(<<"last_trade_time">>, GeneralInfo)
					},
					pourse_stock_controller:add_general_data(StockId, NewGeneralData),

					LastPrice = case LastGeneralData of
						{error, notfound} -> 0;
						{ok, Data2} -> Data2#stock_general_data.last_trade_price_change
					end,
					NewPrice = NewGeneralData#stock_general_data.last_trade_price_change,
					%PriceDifference = lists:flatten(io_lib:format("~p", [NewPrice - LastPrice])),

					Text = if 
						LastPrice < NewPrice ->
							"قیمت سهام " ++
							lists:flatten(io_lib:format("~p", [NewPrice - LastPrice])) ++
							" درصد افزایش یافت";

						LastPrice > NewPrice ->
							"قیمت سهام " ++
							lists:flatten(io_lib:format("~p", [LastPrice - NewPrice])) ++
							" درصد کاهش یافت";
						true -> nil
					end,

					case LastPrice == NewPrice of
						true -> void;
						false ->
							?add_notif_job(
							fun() ->
								pourse_notification_utils:create_notification(
									stock,
									StockData#stock_data.id,
									unicode:characters_to_list(StockData#stock_data.symbol),
									Text
								)
							end
						)
					end
			end
	end,
	ok.



update_stock_comments() ->
	?add_job(fun() -> update_stock_comments_job() end).

update_stock_comments_job() ->
	Now = pourse_datetime_utils:timestamp() * 1000,
	Comments = maps:get(
		<<"comments">>,
		pourse_scraper:scrape_comments(Now, 0)
	),

	lists:foreach(
		fun(Comment) ->
			Symbols = maps:get(<<"symbols">>, Comment),
			StockIds = lists:filtermap(
				fun(Sym) ->
					ClearSym = <<Sym/binary, "1">>,
					pourse_stock_controller:exists(symbol, ClearSym)
				end,
				Symbols
			),
			CleanStockIds = sets:to_list(sets:from_list(StockIds)),

			NewComment = #external_comment_data{
				source	= maps:get(<<"source">>, Comment),
				text	= maps:get(<<"text">>, Comment),
				time	= maps:get(<<"time">>, Comment)
			},

			lists:foreach(
				fun(StockId) ->
					pourse_external_comment_controller:create(StockId, NewComment)
				end,
				CleanStockIds
			)
		end,
		Comments
	),
	ok.



update_news(CheckOldNews, Count) ->
	?add_job(fun() -> update_all_news_job(CheckOldNews, Count) end).

update_all_news_job(CheckOldNews, Count) ->
	NewsList = maps:get(
		<<"news">>, 
		pourse_scraper:scrape_news_list(Count)
	),

	lists:foreach(
		fun(#{<<"title">> := NTitle, <<"url">> := NUrl}) ->
			case pourse_news_controller:exists(title, NTitle) of
				{true, _Id} when (not CheckOldNews) ->
					void;
				{true, _Id} ->
					?add_job(fun() -> update_news_job(NTitle, NUrl) end);
				false ->
					?add_job(fun() -> update_news_job(NTitle, NUrl) end)
			end
		end,
		NewsList
	).

update_news_job(NTitle, NUrl)->
	News = pourse_scraper:scrape_news(NUrl),

	ExternalComments = lists:map(
		fun(Comment) ->
			#external_comment_data{
				source	= maps:get(<<"source">>, Comment),
				text	= maps:get(<<"text">>, Comment),
				time	= maps:get(<<"time">>, Comment)
			}
		end,
		maps:get(<<"external_comments">>, News)
	),

	NewsId = case pourse_news_controller:exists(title, NTitle) of
		{true, Id} -> Id;
		false ->
			NewsData = #news_data{
				title				= maps:get(<<"title">>, News),
				source				= maps:get(<<"source">>, News),
				text				= maps:get(<<"text">>, News),
				time				= maps:get(<<"time">>, News),
				images				= maps:get(<<"images">>, News)
			},
			pourse_news_controller:create(NewsData)
	end,

	lists:foreach(
		fun(C) ->
			pourse_external_comment_controller:create(NewsId, C)
		end,
		ExternalComments
	),
	ok.



update_bourse_status() ->
	?add_job(fun() -> update_bourse_status_job() end).

update_bourse_status_job() ->
	Status = pourse_scraper:scrape_bourse_status(),
	LastData = pourse_bourse_status_controller:get_last(),
	IsNew =
	case LastData of
		{error, notfound} -> true;
		{ok, Data} ->
			LastDateTime = maps:get(<<"time">>, Status),
			case LastDateTime > Data#bourse_status_data.time of
				true -> true;
				false -> false
			end
	end,
	case IsNew of
		false -> void;
		true ->
			StatusData = #bourse_status_data{
				tedpix		= maps:get(<<"tedpix">>, Status),
				volume		= maps:get(<<"volume">>, Status),
				number		= maps:get(<<"number">>, Status),
				value		= maps:get(<<"value">>, Status),
				time		= maps:get(<<"time">>, Status)
			},
			pourse_bourse_status_controller:create(StatusData)
	end,
	ok.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


init(_Args) ->
	erlang:send_after(15 * 1000, self(), category),
	erlang:send_after(60 * 1000, self(), stock),
	erlang:send_after(5 * 60 * 1000, self(), stock_comment),
	erlang:send_after(5 * 60 * 1000, self(), news),
	erlang:send_after(3600 * 1000, self(), old_news),
	erlang:send_after(30 * 1000, self(), bourse_status),
	{ok, nil}.



handle_info(category, State) ->
	update_categories(),
	erlang:send_after(3600 * 1000, self(), category),
	{noreply, State};


handle_info(stock, State) ->
	update_stocks(),
	erlang:send_after(10 * 60 * 1000, self(), stock),
	{noreply, State};


handle_info(stock_comment, State) ->
	update_stock_comments(),
	erlang:send_after(5 * 60 * 1000, self(), stock_comment),
	{noreply, State};


handle_info(news, State) ->
	update_news(false, 1),
	erlang:send_after(10 * 60 * 1000, self(), news),
	{noreply, State};


handle_info(old_news, State) ->
	update_news(true, 82),
	erlang:send_after(3600 * 1000, self(), old_news),
	{noreply, State};


handle_info(bourse_status, State) ->
	update_bourse_status(),
	erlang:send_after(5 * 60 * 1000, self(), bourse_status),
	{noreply, State}.



handle_call(_Request, _From, State) ->
	{reply, ok, State}.


handle_cast(_Msg, State) ->
	{noreply, State}.


terminate(_Reason, _State) ->
	ok.


code_change(_OldVsn, State, _Extra) ->
	{ok, State}.
