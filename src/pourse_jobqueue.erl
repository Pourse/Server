-module(pourse_jobqueue).

-export([
	start/0,
	add_queue/1,
	add_queue/2,
	add_job/2,
	start_worker/1,
	run/2,
	get_job_id/1
]).

-include("config.hrl").


start() ->
	application:start(jobs),
	ok.


add_queue(QueueName) ->
	add_queue(QueueName, ?env(jobqueue)).


add_queue(QueueName, Options) ->
	Node = pourse_node_manager:get_jobqueue_node(),
	rpc:call(Node, jobs, add_queue, [QueueName, Options]).


add_job(QueueName, Job) ->
	pourse_jobqueue_logger:log_add_job(Job),

	Node = pourse_node_manager:get_main_node(),
	JobId = get_job_id(Job),

	rpc:call(Node, pourse_jobqueue_sup, terminate_child, [JobId]),
	rpc:call(Node, pourse_jobqueue_sup, delete_child, [JobId]),
	rpc:call(Node, pourse_jobqueue_sup, start_child, [JobId, [QueueName, Job]]),
	ok.



start_worker(Args) ->
	Node = pourse_node_manager:get_jobqueue_node(),
	Pid = spawn_link(Node, pourse_jobqueue, run, Args),
	{ok, Pid}.


run(QueueName, Job) ->
	CountExceptions = pourse_jobqueue_logger:get_count_exceptions(Job),
	case CountExceptions >= 3 of
		true -> exit(normal);
		false -> pourse_jobqueue_logger:log_run_job(Job)
	end,
	jobs:run(QueueName, Job),
	pourse_jobqueue_logger:log_finish_job(Job).


get_job_id(Job) ->
	erlang:phash2(Job).
