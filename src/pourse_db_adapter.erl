-module(pourse_db_adapter).

-export([
	start/0,
	ensure_all_started/0,
	new/0,
	get/3,
	get_index/4,
	get_index/5,
	get_index/7,
	put/4,
	put/6,
	delete_all/1,
	delete/2,
	delete/3,
	list_keys/2,
	get_value/1,
	update/2,
	add_index/3,
	update_index/4,
	delete/1
]).


-include("config.hrl").



start() ->
	ok = application:start(riakpool),
	ok = start_riakpool(),
	ok.


ensure_all_started() ->
	application:start(riakpool),
	start_riakpool(),
	ok.


new() ->
	{error, pool_already_started} = start_riakpool(),
	{?MODULE}.


start_riakpool() ->
	riakpool:start_pool(?env(database, host), ?env(database, port)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

get(Bucket, Key, {?MODULE}) ->
	{ok, Res} = riakpool:execute(fun(Pid) -> riakc_pb_socket:get(Pid, Bucket, Key) end),
	{?MODULE, Res}.


get_index(Bucket, IndexName, Value, {?MODULE}) ->
	case typeof(Value) of
		"bin" -> IndexType = binary_index;
		"int" -> IndexType = integer_index
	end,
	{ok, {ok, {index_results_v1, Result, _, _}}} = 
		riakpool:execute(
			fun(Pid) ->
				riakc_pb_socket:get_index(
					Pid,
					Bucket,
					{IndexType, IndexName},
					Value
				)
			end
		),
	{ok, Result}.


get_index(Bucket, IndexName, StartRange, EndRange, {?MODULE}) ->
	case typeof(StartRange) of
		"bin" -> IndexType = binary_index;
		"int" -> IndexType = integer_index
	end,
	{ok, {ok, {index_results_v1, Result, undefined, undefined}}} = 
		riakpool:execute(
			fun(Pid) ->
				riakc_pb_socket:get_index(
					Pid,
					Bucket,
					{IndexType, IndexName},
					StartRange, EndRange
				)
			end
		),
	{ok, Result}.


get_index(Bucket, IndexName, StartRange, EndRange, MaxResult, Continuation, {?MODULE}) ->
	case typeof(StartRange) of
		"bin" -> IndexType = binary_index;
		"int" -> IndexType = integer_index
	end,
	{ok, {ok, {index_results_v1, Result, undefined, NewContinuation}}} = 
		riakpool:execute(
			fun(Pid) ->
				riakc_pb_socket:get_index_range(
					Pid,
					Bucket,
					{IndexType, IndexName},
					StartRange, EndRange,
					[
						{max_results, MaxResult},
						{continuation, Continuation}
					]
				)
			end
		),
	{ok, Result, NewContinuation}.


put(Bucket, Key, Value, {?MODULE}) ->
	%put(Bucket, Key, Value, [], STATE).
	Object = riakc_obj:new(Bucket, Key, Value),
	{ok, ok} = riakpool:execute(fun(Pid) -> riakc_pb_socket:put(Pid, Object) end),
	{?MODULE, {ok, Object}}.


put(Bucket, Key, Value, IndexName, IndexVal, {?MODULE}) ->
	Object = riakc_obj:new(Bucket, Key, Value),
	add_index(IndexName, IndexVal, {?MODULE, {ok, Object}}).


delete_all({?MODULE}) ->
	{ok, {ok, Buckets}} = riakpool:execute(fun(Pid) -> riakc_pb_socket:list_buckets(Pid) end),
	lists:foreach(
		fun(B) ->
			case lists:prefix("Pourse_", binary_to_list(B)) of
				true -> delete(B, {?MODULE});
				false -> void
			end
		end,
		Buckets
	),
	ok.


delete(Bucket, {?MODULE}) ->
	Keys = list_keys(Bucket, {?MODULE}),
	lists:foreach(fun(Key) -> delete(Bucket, Key, {?MODULE}) end, Keys),
	ok.


delete(Bucket, Key, {?MODULE}) ->
	ok = riakpool_client:delete(Bucket, Key).


list_keys(Bucket, {?MODULE}) ->
	{ok, {ok, Res}} = riakpool:execute(fun(Pid) -> riakc_pb_socket:list_keys(Pid, Bucket) end),
	Res.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

get_value({?MODULE, {ok, Object}}) ->
	Value = binary_to_term(riakc_obj:get_value(Object)),
	{ok, Value};

get_value({?MODULE, {error, Reason}}) ->
	{error, Reason}.


update(NewValue, {?MODULE, {ok, Object}}) ->
	Updated = riakc_obj:update_value(Object, NewValue),
	{ok, ok} = riakpool:execute(fun(Pid) -> riakc_pb_socket:put(Pid, Updated) end),
	{?MODULE, {ok, Updated}}.


add_index(IndexName, IndexVal, {?MODULE, {ok, Object}}) ->
	IndexType = case typeof(IndexVal) of
		"bin" -> binary_index;
		"int" -> integer_index
	end,
	Meta = riakc_obj:get_update_metadata(Object),
	MD = riakc_obj:set_secondary_index(Meta,[{{IndexType, IndexName}, [IndexVal]}]),
	O = riakc_obj:update_metadata(Object, MD),
	{ok, ok} = riakpool:execute(fun(Pid) -> riakc_pb_socket:put(Pid, O) end),
	{?MODULE, {ok, O}}.


update_index(IndexName, OldValue, NewValue, {?MODULE, {ok, Object}}) ->
	Meta = riakc_obj:get_update_metadata(Object),
	F = fun({A, B}) ->
			case A =:= list_to_binary(IndexName ++ "_" ++ typeof(OldValue)) andalso B =:= binof(OldValue) of
				false ->
					{A, B};
				true ->
					{IndexName ++ "_" ++ typeof(NewValue), NewValue}
			end
		end,
	NewMeta = dict:update(<<"index">>, fun(X) -> lists:map(F, X) end, Meta),
	O = riakc_obj:update_metadata(Object, NewMeta),
	{ok, ok} = riakpool:execute(fun(Pid) -> riakc_pb_socket:put(Pid, O) end),
	{?MODULE, {ok, O}}.


delete({?MODULE, {ok, Object}}) ->
	{ok, ok} = riakpool:execute(fun(Pid) -> riakc_pb_socket:delete_obj(Pid, Object) end),
	ok.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

typeof(X) when is_binary(X) -> "bin";
typeof(X) when is_integer(X) -> "int".


binof(X) when is_binary(X) -> X;
binof(X) when is_integer(X) -> integer_to_binary(X).
