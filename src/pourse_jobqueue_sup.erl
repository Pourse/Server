-module(pourse_jobqueue_sup).

-behaviour(supervisor).

-export([
	start_link/0,
	init/1,
	start_child/2,
	terminate_child/1,
	delete_child/1,
	count_children/0
]).


start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).


init(_Args) ->
	SupFlags = #{strategy => one_for_one, intensity => 100000, period => 20},
	{ok, {SupFlags, []}}.


start_child(Id, Args) ->
	ChildSpecs = #{
		id => Id,
		start => {pourse_jobqueue, start_worker, [Args]},
		restart => transient,
		shutdown => brutal_kill,
		type => worker,
		modules => [pourse_jobqueue]
	},
	supervisor:start_child(?MODULE, ChildSpecs).


terminate_child(Id) ->
	supervisor:terminate_child(?MODULE, Id).


delete_child(Id) ->
	supervisor:delete_child(?MODULE, Id).


count_children() ->
	supervisor:count_children(?MODULE).
