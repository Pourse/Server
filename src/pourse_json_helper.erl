-module(pourse_json_helper).

-export([
	start/0,
	upgrade_code/0,
	loop/1,
	to_json/1,
	to_json/2,
	from_json/1,
	from_json/2
]).


start() ->
	true = register(pourse_json_helper, spawn(fun() -> loop(get_encoder_decoder()) end)),
	ok.


upgrade_code() ->
	pourse_json_helper ! upgrade.


loop({Encoder, Decoder} = ED) ->
	receive
		{encode, Pid, Record} ->
			spawn(
				fun() -> 
					Json = Encoder(Record),
					Pid ! {json, Json}
				end
			),
			loop(ED);

		{decode, Pid, Json} ->
			spawn(
				fun() -> 
					Record = Decoder(Json),
					Pid ! {json, Record}
				end
			),
			loop(ED);

		upgrade ->
			code:purge(?MODULE),
			code:load_file(?MODULE),
			?MODULE:loop(get_encoder_decoder())
	end.


get_encoder_decoder() ->
	Records = sets:to_list(sets:from_list(find_records())),
	Encoder = jsonx:encoder(Records, [{ignore, [undefined]}]),
	Decoder = jsonx:decoder(Records),
	{Encoder, Decoder}.


find_records() ->
	{ok, Modules} = file:list_dir(filename:dirname(code:which(?MODULE))),
	RecInformations = lists:filtermap(
		fun(X) ->
			case lists:suffix(".beam", X) of
				true ->
					ModName = list_to_atom(string:substr(X, 1,  length(X) - 5)),
					case lists:keyfind(record_info, 1, ModName:module_info(exports)) of
						{record_info, 1} ->
							Records = record_info:record_list(ModName),
							RecFields = lists:map(
								fun(Rec) ->
									{Rec, record_info:field_list(Rec, ModName)}
								end,
								Records
							),
							{true, RecFields};

						false -> false
					end;
				false -> false
			end
		end,
		Modules
	),
	lists:concat(RecInformations).



to_json(Map, pretty) when is_map(Map) ->
	jiffy:encode(Map, [pretty]).


to_json(Map) when is_map(Map) ->
	jiffy:encode(Map);

to_json(Record) ->
	pourse_json_helper ! {encode, self(), Record},
	receive
		{json, Json} -> Json
	end.


from_json(Json) ->
	pourse_json_helper ! {decode, self(), Json},
	receive
		{json, Record} -> Record
	end.


from_json(Json, map) ->
	jiffy:decode(Json, [return_maps]).
