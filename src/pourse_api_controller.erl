-module(pourse_api_controller).

-export([
	parse_transform/2,
	handle_http_req/1,
	abort/1,
	abort/2,
	get_request/0,
	get_request/1,
	get_request/2
]).

-import(pourse_json_helper, [
	from_json/1,
	to_json/1
]).


parse_transform(Forms, _Options) ->
	case re:run(erlang:element(1, erlang:element(4, lists:nth(1, Forms))), ".*pourse_api_.*_controller.erl") of
		nomatch -> Forms;
		{match, _} ->
			Clauses = lists:filtermap(
				fun(Line) ->
					case erlang:element(1, Line) =:= function of
						false -> false;
						true ->
							Function = atom_to_list(erlang:element(3, Line)),
							RF = re:run(Function, "^handle_.*"),
							case RF of
								nomatch -> false;
								{match, _} ->
									Clause = lists:nth(1, erlang:element(5, Line)),
									NewClause = 
									case erlang:element(4, Line) =:= 2 of
										false -> Clause;
										true ->
											Args = erlang:element(3, Clause),
											LineNumber = erlang:element(2, lists:nth(1, Args)),
											NewArgs = Args ++ [{atom, LineNumber, none}],
											erlang:setelement(3, Clause, NewArgs)
									end,
									{true, NewClause}
							end
					end
				end,
				Forms
			),
			CleanForms = lists:filtermap(
				fun(Line) ->
					case erlang:element(1, Line) =:= function of
							false -> {true, Line};
							true ->
								Function = atom_to_list(erlang:element(3, Line)),
								RF = re:run(Function, "^handle_.*"),
								case RF of
									nomatch -> {true, Line};
									{match, _} -> false
								end
					end
				end,
				Forms
			),
			case length(Clauses) =:= 0 of
				true -> Forms;
				false ->
					FirstLine = erlang:element(2, lists:nth(1, Clauses)),
					[Eof | Tail] = lists:reverse(CleanForms),
					lists:append([lists:reverse(Tail), [{function, FirstLine, handle, 3, Clauses}], [Eof]])
			end
	end.



find_api_resources() ->
	{ok, Files} = file:list_dir(filename:join(filename:dirname(code:which(?MODULE)), "../src/api")),
	lists:filtermap(
		fun(X) ->
			case lists:prefix("pourse_api_", X) andalso lists:suffix("_controller.erl", X) of
				true ->
					{true, string:substr(X, 12,  length(X) - 26)};
				false -> false
			end
		end,
		Files
	).


get_resource_module(Resource) ->
	list_to_atom("pourse_api_" ++ Resource ++ "_controller").



handle_http_req(Req) ->
	Resources = find_api_resources(),
	{Pid, Ref} = spawn_monitor(fun() -> handle(Resources, Req) end),

	receive
		{'DOWN', Ref, process, Pid, Why} ->
			case Why of
				normal ->
					void;
				{function_clause, [{_, handle, _, _} | _]} ->
					abort(Req, 404, url_or_method_not_found);
				{{case_clause, [_ | _]}, [{_, handle, _, _} | _]} ->
					abort(Req, 404, url_or_method_not_found);
				{{badmatch, badtoken}, _} ->
					abort(Req, 401, bad_or_expired_token);
				{{assertEqual, [{module, pourse_auth} | _]}, _} ->
					abort(Req, 403, bad_admin_key);
				{{badmatch, false}, [{_, handle, _, _} | _]} ->
					abort(Req, 404, url_or_method_not_found);
				{{assertNotEqual, [{module, pourse_validator} | _]}, _} ->
					abort(Req, 400);
				{badarg, [{uuid, to_binary, _, _} | _]} ->
					abort(Req, 400);
				{{badmatch, _}, [{pourse_validator, validate_schema, 1, _} | _]} ->
					abort(Req, 400)
			end
	end.


handle(Resources, Req) ->
	self() ! {req, Req},

	Method = Req:get(method),
	case Method of
		'OPTIONS' ->
			respond(Req, 200, "");
		_ ->
			case Req:resource([urldecode]) of
				["api", Resource | UrlTail] ->
					true = lists:member(Resource, Resources),
					ResourceModule = get_resource_module(Resource),

					Json = get_request(json),
					{Template, StatusCode} = ResourceModule:handle(Method, UrlTail, Json),
					respond(Req, StatusCode, Template);

				["doc"] when Method =:= 'GET' ->
					Req:redirect("/static/apidoc/index.html");

				["doc", "api.json"] when Method =:= 'GET' ->
					respond(Req, 200, pourse_doc_generator:get_api_doc())
			end
	end.



respond(Req, StatusCode, Template) ->
	Req:respond(
		StatusCode,
		[
			{"Content-Type", "application/json"},
			{"Access-Control-Allow-Origin", "*"},
			{"Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Token"}
		],
		to_json(Template)
	).



abort(StatusCode) ->
	abort(StatusCode, <<"">>).


abort(StatusCode, Reason) when is_integer(StatusCode) ->
	Req = get_request(),
	abort(Req, StatusCode, Reason);

abort(Req, StatusCode) ->
	abort(Req, StatusCode, <<"">>).


abort(Req, StatusCode, Reason) ->
	R = case is_list(Reason) of
		true -> list_to_binary(Reason);
		false -> Reason
	end,
	respond(Req, StatusCode, #{error => R}),
	exit(normal).



get_request() -> 
	receive
		{req, Req} ->
			self() ! {req, Req},
			Req
	end.


get_request(args = _Attribute) -> 
	Req = get_request(),
	Req:parse_qs(unicode);

get_request(json = _Attribute) -> 
	Req = get_request(),
	Body = unicode:characters_to_binary(Req:get(body)),
	case from_json(Body) of
		<<>> -> none;
		{error, _, _} -> none;
		Json -> Json
	end;

get_request(headers = _Attribute) -> 
	Req = get_request(),
	Req:get(headers).


get_request(headers = _Attribute, Parameter) ->
	Req = get_request(),
	Req:get_variable(Parameter, Req:get(headers)).
