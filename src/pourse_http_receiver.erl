-module(pourse_http_receiver).

-export([
	start/0,
	stop/0,
	upgrade_code/0
]).

-include("config.hrl").


start() ->
	ssl:start(),
	{ok, _Pid} = misultin:start_link([
		{port, ?env(api, port)},
		{static, "statics"},
		{loop, fun(Req) -> pourse_api_controller:handle_http_req(Req) end},
		{ssl, [
			{certfile, "priv/ssl/certs/cert.pem"},
			{keyfile, "priv/ssl/certs/key.pem"}
		]}
	]),
	ok.


stop() ->
	misultin:stop().


upgrade_code() ->
	void.
