-module(pourse_jobqueue_logger).

-export([
	insert/1,
	lookup/1,
	local_insert/1,
	local_lookup/1
]).

-export([
	log_add_job/1,
	log_run_job/1,
	log_finish_job/1,
	get_count_exceptions/1
]).

-import(pourse_jobqueue, [
	get_job_id/1
]).

-include("config.hrl").

-define(FILENAME, ?env(file, jobqueue_log_file)).
-define(TABLE, ?MODULE).


log_add_job(Job) ->
	Id = get_job_id(Job),
	Info = erlang:fun_info(Job),
	insert({Id, Info, -1}).


log_run_job(Job) ->
	Id = get_job_id(Job),
	[{Id, Info, CountExceptions}] = lookup(Id),
	insert({Id, Info, CountExceptions + 1}).


log_finish_job(Job) ->
	Id = get_job_id(Job),
	[{Id, Info, _}] = lookup(Id),
	insert({Id, Info, -2}).


get_count_exceptions(Job) ->
	Id = get_job_id(Job),
	[{Id, _, CountExceptions}] = lookup(Id),
	CountExceptions.



open() ->
	{ok, ?TABLE} = dets:open_file(?TABLE, [{type, set}, {file, ?FILENAME}]).


close() ->
	dets:close(?TABLE).


insert(Data) ->
	Node = pourse_node_manager:get_main_node(),
	rpc:call(Node, ?MODULE, local_insert, [Data]).


lookup(Id) ->
	Node = pourse_node_manager:get_main_node(),
	rpc:call(Node, ?MODULE, local_lookup, [Id]).


local_insert(Data) ->
	open(),
	dets:insert(?TABLE, Data),
	close().


local_lookup(Id) ->
	open(),
	Data = dets:lookup(?TABLE, Id),
	close(),
	Data.
