-module(pourse_node_manager).

-export([
	start/0,
	get_main_node/0,
	get_jobqueue_node/0
]).

-include("config.hrl").

start() ->
	start_main_node(),
	start_jobqueue_node(),
	ok.



start_main_node() ->
	net_kernel:start([get_main_node(), longnames]),
	true = erlang:set_cookie(node(), get_cookie()),
	ok.


start_jobqueue_node() ->
	slave:start(get_jobqueue_node_ip(), get_jobqueue_node_name(), get_slave_args()),
	ok = rpc:call(get_jobqueue_node(), pourse, start_jobqueue_node, []),
	ok.



get_main_node() ->
	list_to_atom(atom_to_list(get_main_node_name()) ++ "@" ++ atom_to_list(get_main_node_ip())).


get_main_node_name() ->
	'pourse_main'.


get_main_node_ip() ->
	?env(node, main_node_ip).



get_jobqueue_node() ->
	list_to_atom(atom_to_list(get_jobqueue_node_name()) ++ "@" ++ atom_to_list(get_jobqueue_node_ip())).


get_jobqueue_node_name() ->
	'pourse_jobqueue'.


get_jobqueue_node_ip() ->
	?env(node, jobqueue_node_ip).



get_slave_args() ->
	Cookie = atom_to_list(get_cookie()),
	{Min, Max} = get_listen_ports(),
	MinS = integer_to_list(Min),
	MaxS = integer_to_list(Max),

	string:join([
		"-pa", "ebin", "deps/*/ebin",
		"-setcookie", Cookie,
		"-config", "app.config",
		"-kernel", "inet_dist_listen_min", MinS, "inet_dist_listen_max", MaxS
	], " ").


get_cookie() ->
	?env(node, cookie).


get_listen_ports() ->
	{ok, Min} = application:get_env(kernel, inet_dist_listen_min),
	{ok, Max} = application:get_env(kernel, inet_dist_listen_max),
	{Min, Max}.
