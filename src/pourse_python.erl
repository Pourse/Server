-module(pourse_python).

-export([
	call/3
]).

-include("config.hrl").

-define(PYTHON_OPTIONS, ?env(python_opts)).


call(Module, Function, Args) ->
	{ok, P} = python:start_link(?PYTHON_OPTIONS),
	Mod = list_to_atom("py_src." ++ atom_to_list(Module)),
	Result = python:call(P, Mod, Function, Args),
	python:stop(P),
	Result.
