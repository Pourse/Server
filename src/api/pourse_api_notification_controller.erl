-module(pourse_api_notification_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-import(pourse_notification_utils,[
	add_observer/3,
	delete_observer/3
]).

-include("auth.hrl").
-include("data.hrl").
-include("data/notification_data.hrl").
-include("data/user_data.hrl").
-include("api/api.hrl").
-include("api/info/notification_info.hrl").


handle_enable_notification('POST', ["stock", StockId]) ->
	UserId = ?authenticate,

	{ok, UserData} = pourse_user_controller:get(id, UserId),
	ObservableList =  UserData#user_data.observables,

	StockIdBin = list_to_binary(StockId),
	Observables =
	case lists:member(StockIdBin, ObservableList) of
		true -> ObservableList;
		false ->
			try add_observer(stock, StockIdBin, UserData#user_data.onesignal_ids) of
				ok -> void
			catch
				error: {badmatch,{error,notfound}} -> ?abort(404, "Stock not found")
			end,

			UpdatedObservableList = [StockIdBin | ObservableList],
			UpdatedUser = UserData#user_data{observables = UpdatedObservableList},
			pourse_user_controller:update(UpdatedUser),
			UpdatedObservableList
	end,

	ObservablesInfo = #enabled_notifications_info{observables = Observables},
	{ObservablesInfo#enabled_notifications_info{}, 200}.


handle_disable_notification('DELETE', ["stock", StockId]) ->
	UserId = ?authenticate,

	{ok, UserData} = pourse_user_controller:get(id, UserId),
	StockIdBin = list_to_binary(StockId),

	try add_observer(stock, StockIdBin, UserData#user_data.onesignal_ids) of
		ok -> void
	catch
		error: {badmatch,{error,notfound}} -> ?abort(404, "Stock not found")
	end,

	ObservableList =  UserData#user_data.observables,
	ObservableList =  UserData#user_data.observables,
	UpdatedObservableList = lists:delete(StockIdBin, ObservableList),
	UpdatedUser = UserData#user_data{observables = UpdatedObservableList},

	pourse_user_controller:update(UpdatedUser),
	delete_observer(stock, StockIdBin, UserData#user_data.onesignal_ids),

	ObservablesInfo = #enabled_notifications_info{observables = UpdatedObservableList},
	{ObservablesInfo#enabled_notifications_info{}, 200}.


handle_get_my_enabled_notifications('GET', ["stock"]) ->
	UserId = ?authenticate,
	{ok, UserData} = pourse_user_controller:get(id, UserId),
	Observables = UserData#user_data.observables,

	ObservablesInfo = #enabled_notifications_info{observables = Observables},
	{ObservablesInfo#enabled_notifications_info{}, 200}.
