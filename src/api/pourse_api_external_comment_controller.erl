-module(pourse_api_external_comment_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/external_comment_data.hrl").
-include("api/api.hrl").
-include("api/info/external_comment_info.hrl").


handle_get_all_external_comments('GET', [OwnerId]) ->
	?authenticate,
	{ST, ET, MR, CT} = ?pagination_args,

	Id = list_to_binary(OwnerId),
	{AllExternalCommentsData, Continuation} = pourse_external_comment_controller:get(Id, ST, ET, MR, CT),

	AllExternalCommentsInfo = #external_comment_info{
		comments = AllExternalCommentsData,
		continuation = Continuation
	},

	{AllExternalCommentsInfo#external_comment_info{}, 200}.
