-module(pourse_api_bourse_status_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/bourse_status_data.hrl").
-include("api/api.hrl").
-include("api/info/bourse_status_info.hrl").


handle_get_last_status_info('GET', ["last"]) ->
	?authenticate,

	LastStatusData = case pourse_bourse_status_controller:get_last() of
		{error, notfound} -> null;
		{ok, D} -> D
	end,

	LastStatusInfo = #bourse_status_info{
		status = LastStatusData
	},

	{LastStatusInfo#bourse_status_info{}, 200}.


handle_get_all_status_info('GET', []) ->
	?authenticate,
	{ST, ET, MR, CT} = ?pagination_args,

	{AllStatusData, Continuation} = pourse_bourse_status_controller:get(ST, ET, MR, CT),

	StatusListInfo = #bourse_status_list_info{
		status			= AllStatusData,
		continuation	= Continuation
	},

	{StatusListInfo#bourse_status_list_info{}, 200}.
