-module(pourse_api_internal_comment_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("validator.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/user_data.hrl").
-include("data/internal_comment_data.hrl").
-include("api/api.hrl").
-include("api/info/internal_comment_info.hrl").
-include("api/form/comment_form.hrl").


handle_leave_comment('POST', [OwnerId], Json) ->
	UserId = ?authenticate,
	?validate_schema(comment_form),

	{ok, UserData} = pourse_user_controller:get(id, UserId),
	Username = UserData#user_data.username,

	Comment = #internal_comment_data{
		user_id		= UserId,
		username	= Username,
		time		= pourse_datetime_utils:timestamp(),
		text		= Json#comment_form.text
	},

	Id = list_to_binary(OwnerId),
	CId = pourse_internal_comment_controller:create(Id, Comment),
	{Comment#internal_comment_data{id = CId}, 200}.


handle_get_all_internal_comments('GET', [OwnerId]) ->
	?authenticate,
	{ST, ET, MR, CT} = ?pagination_args,

	Id = list_to_binary(OwnerId),
	{AllInternalCommentsData, Continuation} = pourse_internal_comment_controller:get(Id, ST, ET, MR, CT),

	AllInternalCommentsInfo = #internal_comment_info{
		comments = AllInternalCommentsData,
		continuation = Continuation
	},

	{AllInternalCommentsInfo#internal_comment_info{}, 200}.


handle_delete_comment('DELETE', [OwnerId, CommentId]) ->
	UserId = ?authenticate,
	OId = list_to_binary(OwnerId),
	CId = list_to_binary(CommentId),

	CommentData =
	case pourse_internal_comment_controller:get(OId, CId) of
		{error, notfound} -> ?abort(404, "Comment not found");
		{ok, Data} -> Data
	end,

	case CommentData#internal_comment_data.user_id =:= UserId of
		false -> ?abort(403, "You don't have permission to delete this comment");
		true -> void
	end,

	pourse_internal_comment_controller:delete(OId, CId),
	{'', 200}.


handle_disapprove('DELETE', ["disapprove", OwnerId, CommentId]) ->
	?authorize,
	OId = list_to_binary(OwnerId),
	CId = list_to_binary(CommentId),

	CommentData =
	case pourse_internal_comment_controller:get(OId, CId) of
		{error, notfound} -> ?abort(404, "Comment not found");
		{ok, Data} -> Data
	end,

	UpdatedCommentData = CommentData#internal_comment_data{
		approved = false
	},
	pourse_internal_comment_controller:update(OId, UpdatedCommentData),

	{'', 200}.
