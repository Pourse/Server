-module(pourse_api_bug_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("validator.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/user_data.hrl").
-include("data/bug_data.hrl").
-include("api/api.hrl").
-include("api/form/bug_form.hrl").
-include("api/info/bug_info.hrl").
-include("api/info/user_info.hrl").
-include("api/info/empty_info.hrl").


handle_report_bug('POST', [], Json) ->
	UserId = ?authenticate,
	?validate_schema(bug_form),

	BugData = #bug_data{
		user_id			= UserId,
		title			= Json#bug_form.title,
		details			= Json#bug_form.details,
		time			= pourse_datetime_utils:timestamp()
	},

	pourse_bug_controller:create(BugData),
	{#empty_info{}, 200}.


handle_get_all_bugs('GET', []) ->
	?authorize,
	{ST, ET, MR, CT} = ?pagination_args,

	{AllBugsData, Continuation} = pourse_bug_controller:get(ST, ET, MR, CT),

	AllBugsInfo = lists:map(
		fun(BD) ->
			{ok, UserData} = pourse_user_controller:get(id, BD#bug_data.user_id),

			#bug_info{
				id = BD#bug_data.id,
				title = BD#bug_data.title,
				user = #user_info{
					id = BD#bug_data.user_id,
					username = UserData#user_data.username,
					email = UserData#user_data.email
				},
				details = BD#bug_data.details,
				time = BD#bug_data.time,
				checked = BD#bug_data.checked
			}
		end,
		AllBugsData
	),

	BugsInfoList = #bug_info_list{
		bugs = AllBugsInfo,
		continuation = Continuation
	},

	{BugsInfoList#bug_info_list{}, 200}.


handle_check_bug('POST', [BugId]) ->
	?authorize,
	Id = list_to_binary(BugId),

	BugData =
	case pourse_bug_controller:get(Id) of
		{error, notfound} -> ?abort(404, "Bug not found");
		{ok, Data} -> Data
	end,

	UpdatedBugData = BugData#bug_data{
		checked = true
	},

	pourse_bug_controller:update(UpdatedBugData),
	{#empty_info{}, 200}.
