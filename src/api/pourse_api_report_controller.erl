-module(pourse_api_report_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("validator.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/user_data.hrl").
-include("data/report_data.hrl").
-include("api/api.hrl").
-include("api/form/report_form.hrl").
-include("api/info/report_info.hrl").
-include("api/info/empty_info.hrl").


handle_report('POST', [], Json) ->
	UserId = ?authenticate,
	?validate_schema(report_form),

	{ok, UserData} = pourse_user_controller:get(id, UserId),

	Now = pourse_datetime_utils:timestamp(day),
	ReportCount = UserData#user_data.report_count,

	case UserData#user_data.last_report_day == Now of
		true ->
			case ReportCount < UserData#user_data.max_reports of
				false ->
					?abort(406, "You can't report more");
				true ->
					UpdatedUserData = UserData#user_data{
						report_count = ReportCount + 1
					},
					pourse_user_controller:update(UpdatedUserData),
					true
			end;
		false ->
			UpdatedUserData = UserData#user_data{
				report_count = 1,
				last_report_day = Now
			},
			pourse_user_controller:update(UpdatedUserData),
			true
	end,

	ReportData = #report_data{
		owner_id		= Json#report_form.owner_id,
		comment_id		= Json#report_form.comment_id,
		user_id			= UserId,
		reason			= Json#report_form.reason,
		time			= pourse_datetime_utils:timestamp()
	},

	pourse_report_controller:create(ReportData),
	{#empty_info{}, 200}.


handle_get_all_reports('GET', []) ->
	?authorize,
	{ST, ET, MR, CT} = ?pagination_args,

	{AllReportsData, AllCommentsData, Continuation} = pourse_report_controller:get(ST, ET, MR, CT),

	AllReportsInfo = #reports_info{
		reports = AllReportsData,
		comments = AllCommentsData,
		continuation = Continuation
	},

	{AllReportsInfo#reports_info{}, 200}.


handle_check_report('POST', [ReportId]) ->
	?authorize,
	Id = list_to_binary(ReportId),

	ReportData =
	case pourse_report_controller:get(Id) of
		{error, notfound} -> ?abort(404, "Report not found");
		{ok, Data} -> Data
	end,

	UpdatedReportData = ReportData#report_data{
		checked = true
	},
	pourse_report_controller:update(UpdatedReportData),

	{#empty_info{}, 200}.
