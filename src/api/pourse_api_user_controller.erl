-module(pourse_api_user_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-import(pourse_notification_utils,[
	add_observer/3
]).

-include("auth.hrl").
-include("validator.hrl").
-include("data/user_data.hrl").
-include("api/api.hrl").
-include("api/form/signup_form.hrl").
-include("api/form/login_form.hrl").
-include("api/info/user_info.hrl").
-include("api/info/token_info.hrl").


handle_signup('POST', ["signup"], Json) ->
	?validate_schema(signup_form),

	UserId = case pourse_user_controller:get(username, Json#signup_form.username) of
		{error, notfound} ->
			Password = crypto:hash(md5, Json#signup_form.password),

			UserData = #user_data{
				username = Json#signup_form.username, 
				email = Json#signup_form.email,
				password = Password,
				onesignal_ids = [],
				observables = []
			},

			pourse_user_controller:create(UserData);

		{ok, _UserData} ->
			?abort(409, "Username already exists")
	end,

	{ok, Data} = pourse_user_controller:get(id, UserId),
	UserInfo = get_info(Data),
	{UserInfo#user_info{}, 201}.


handle_login('POST', ["login"], Json) ->
	?validate_schema(login_form),

	TokenInfo =
	case pourse_user_controller:get(username, Json#login_form.username) of
		{error, notfound} -> ?abort(404, "User not found");
		{ok, UserData} ->
			case UserData#user_data.password =:= crypto:hash(md5, Json#login_form.password) of
				false -> ?abort(406, "Wrong password");
				true ->
					Token = pourse_auth:add_token(UserData#user_data.id),

					OnesignalIds = UserData#user_data.onesignal_ids,
					case lists:member(Json#login_form.onesignal_id, OnesignalIds) of 
						true -> #token_info{token = Token};
						false -> 
							UpdatedOnesignalIds = [Json#login_form.onesignal_id | OnesignalIds],
							UpdatedUser = UserData#user_data{onesignal_ids = UpdatedOnesignalIds},
							pourse_user_controller:update(UpdatedUser),

							Observables = UserData#user_data.observables,
							lists:foreach(
								fun(X) ->
									add_observer(stock, X, [Json#login_form.onesignal_id])
								end,
								Observables
							),

							#token_info{token = Token}
					end
			end
	end,

	{TokenInfo#token_info{}, 200}.


handle_logout('POST', ["logout"]) ->
	?authenticate,
	pourse_auth:del_token(),
	{'', 200}.


handle_get_profile('GET', []) ->
	UserId = ?authenticate,
	{ok, UserData} = pourse_user_controller:get(id, UserId),
	UserInfo = get_info(UserData),
	{UserInfo#user_info{}, 200}.



get_info(UserData) ->
	#user_info{
		id				= UserData#user_data.id,
		username		= UserData#user_data.username,
		email			= UserData#user_data.email
	}.
