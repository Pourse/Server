-module(pourse_api_stock_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/stock_data.hrl").
-include("api/api.hrl").
-include("api/info/stock_general_info.hrl").


handle_get_last_general_info('GET', [StockId, "last"]) ->
	?authenticate,

	Id = list_to_binary(StockId),
	case pourse_stock_controller:get(id, Id) of
		{error, notfound} -> ?abort(404, "Stock not found");
		{ok, _} -> void
	end,

	LastData = case pourse_stock_controller:get_last_general_data(Id) of
		{error, notfound} -> null;
		{ok, D} -> D
	end,

	General = #stock_last_general_info{
		general_data = LastData
	},

	{General#stock_last_general_info{}, 200}.


handle_get_all_general_info('GET', [StockId]) ->
	?authenticate,
	{ST, ET, MR, CT} = ?pagination_args,

	Id = list_to_binary(StockId),
	{AllGeneralData, Continuation} = pourse_stock_controller:get_general_data(Id, ST, ET, MR, CT),

	AllGeneralInfo = #stock_general_info{
		general_data = AllGeneralData,
		continuation = Continuation
	},

	{AllGeneralInfo#stock_general_info{}, 200}.
