-module(pourse_api_news_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("pagination_utils.hrl").
-include("data.hrl").
-include("data/news_data.hrl").
-include("api/api.hrl").
-include("api/info/news_info.hrl").


handle_get_news_info('GET', [NewsId]) ->
	?authenticate,

	Id = list_to_binary(NewsId),
	NewsGeneralData = 
	case pourse_news_controller:get(id, Id) of
		{error, notfound} -> ?abort(404, "News not found");
		{ok, D} -> D
	end,

	NewsInfo = #news_general_info{
		news = NewsGeneralData
	},

	{NewsInfo#news_general_info{}, 200}.


handle_get_news_list('GET', []) ->
	?authenticate,
	{ST, ET, MR, CT} = ?pagination_args,

	{AllNewsList, Continuation} = pourse_news_controller:get(ST, ET, MR, CT),

	News = lists:map(
		fun(N) ->
			Image = 
			case N#news_data.images of
				[] -> null;
				[H | _]	-> H
			end,

			#news_basic_info{
				id		= N#news_data.id,
				title	= N#news_data.title,
				time	= N#news_data.time,
				image	= Image
			}
		end,
		AllNewsList
	),

	NewsListInfo = #news_list_info{
		news = News,
		continuation = Continuation
	},

	{NewsListInfo#news_list_info{}, 200}.
