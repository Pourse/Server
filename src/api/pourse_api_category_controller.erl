-module(pourse_api_category_controller).

-compile({parse_transform, pourse_api_controller}).

-export([
	handle/3
]).

-include("auth.hrl").
-include("data/category_data.hrl").
-include("data/stock_data.hrl").
-include("api/api.hrl").
-include("api/info/category_info.hrl").


handle_get_all_categories('GET', [Type]) ->
	?authenticate,

	TypeAtom =
	try list_to_existing_atom(Type) of
		X -> X
	catch
		error: badarg -> ?abort(404, "Category does not exist")
	end,

	CategoryData = 
	case pourse_category_controller:get(TypeAtom) of
		{error, notfound} -> ?abort(404, "Category does not exist");
		{ok, Data} -> Data
	end,

	Items = CategoryData#category_data.items,
	CategoryInfo = #category_info{categories = Items},
	{CategoryInfo#category_info{}, 200}.
