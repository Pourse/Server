-module(pourse_notification_utils).

-export([
	create_notification/4,
	add_notification/2,
	add_observer/3,
	delete_observer/3,
	get_observers/2
]).

-import(pourse_json_helper,[
	to_json/1,
	from_json/2
]).

-include("config.hrl").
-include("data.hrl").
-include("api/api.hrl").
-include("data/notification_data.hrl").

-define(APPID, ?env(notification, app_id)).
-define(AUTHORIZATION, ?env(notification, authorization)).
-define(URL, ?env(notification, url)).


create_notification(Tag, OwnerId, Title, Message) ->
	Subscribers = get_observers(Tag, OwnerId),
	case erlang:length(Subscribers) =:= 0 of
		true -> void;
		false ->
			Headers = [
				{"Authorization", ?AUTHORIZATION},
				{"Content-Type", "application/json"}
			],

			Notification = #{
				<<"app_id">>				=> ?APPID,
				<<"include_player_ids">>	=> Subscribers,
				<<"headings">>				=> #{<<"en">> => unicode:characters_to_binary(Title)},
				<<"contents">>				=> #{<<"en">> => unicode:characters_to_binary(Message)}
			},

			{200, _} = send_notification(Headers, to_json(Notification))
	end.


send_notification(Headers, Notification) ->
	{ok, {{"HTTP/1.1", ReturnCode, _}, _, Body}} = httpc:request(
		post,
		{?URL, Headers, "application/json", Notification},
		[],
		[]
	),
	{ReturnCode, from_json(Body, map)}.
	

add_notification(Tag, OwnerId) ->
	pourse_notification_controller:create(Tag, OwnerId, #notification_data{observers=[]}),
	ok.


add_observer(Tag, OwnerId, UserOsIds) ->
	{ok, NotifData} = pourse_notification_controller:get(Tag, OwnerId),
	ObserverList = NotifData#notification_data.observers,

	UpdatedObserverList = lists:merge(UserOsIds, ObserverList),
	UpdatedNotifData = NotifData#notification_data{
		observers = UpdatedObserverList
	},

	pourse_notification_controller:update(UpdatedNotifData),
	ok.


delete_observer(Tag, OwnerId, UserOsIds) ->
	{ok, NotifData} = pourse_notification_controller:get(Tag, OwnerId),
	ObserverList =  NotifData#notification_data.observers,

	UpdatedObserverList = lists:subtract(ObserverList, UserOsIds),
	UpdatedNotifData = NotifData#notification_data{
		observers = UpdatedObserverList
	},

	pourse_notification_controller:update(UpdatedNotifData),
	ok.


get_observers(Tag, OwnerId) ->
	{ok, NotifData} = pourse_notification_controller:get(Tag, OwnerId),
	NotifData#notification_data.observers.
