-module(pourse_doc_generator).

-export([
	start/0,
	upgrade_code/0,
	loop/1,
	get_api_doc/0
]).

-include("config.hrl").
-include("api/api.hrl").


start() ->
	ApiDoc = generate_swagger_doc(),
	true = register(pourse_doc_generator, spawn(fun() -> loop(ApiDoc) end)),
	ok.


upgrade_code() ->
	pourse_doc_generator ! upgrade.


loop(ApiDoc) ->
	receive
		{get_doc, Pid} ->
			spawn(fun() -> Pid ! {doc, ApiDoc} end ),
			loop(ApiDoc);

		upgrade ->
			code:purge(?MODULE),
			code:load_file(?MODULE),
			?MODULE:loop(generate_swagger_doc())
	end.


get_api_doc() ->
	pourse_doc_generator ! {get_doc, self()},
	receive
		{doc, ApiDoc} ->
			Host = list_to_binary(?request(headers, 'Host')),
			ApiDoc#{host => Host}
	end.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

generate_api_doc() ->
	Schemas = get_schemas(),
	Routes = get_routes(),
	{Schemas, Routes}.


generate_swagger_doc() ->
	{Schemas, Routes} = generate_api_doc(),

	Info = #{
		version		=> <<"1.0.0">>,
		title		=> <<"Pourse">>
	},

	Definitions = lists:foldr(
		fun(Schema, Result) ->
			{Name, Properties} = Schema,
			Props = lists:foldr(
				fun(P, Res) ->
					{PropName, PropType} = P,
					PT = case is_list(PropType) of
						false ->
							case PropType of
								{schema, S} ->
									#{<<"$ref">> => list_to_binary("#/definitions/" ++ atom_to_list(S))};
								_ ->
									#{type => PropType}
							end;
						true -> #{
							type => array,
							items => case lists:nth(1, PropType) of
									{schema, S} ->
										#{<<"$ref">> => list_to_binary("#/definitions/" ++ atom_to_list(S))};
									_ ->
										#{type => lists:nth(1, PropType)}
								end
						}

					end,
					Res#{
						PropName => PT
					}
				end,
				#{},
				Properties
			),

			Def = #{
				type => object,
				properties => Props
			},

			Result#{
				Name => Def
			}
		end,
		#{},
		Schemas
	),

	Paths = lists:foldr(
		fun(Resource, Result) ->
			{Tag, ReqList} = Resource,
			lists:foldr(
				fun(Req, Res) ->
					Url = list_to_binary("/" ++ Tag ++ maps:get(url, Req)),
					R = case maps:find(Url, Res) of
						error -> #{};
						{ok, V} -> V
					end,
					Params = case maps:get(reqeust_body, Req) of
						"" -> [];
						B -> [#{
							name => list_to_binary(B),
							in => body,
							required => true,
							schema => #{
								<<"$ref">> => list_to_binary("#/definitions/" ++ B)
							}
						}]
					end,
					UrlParams = lists:map(
						fun(P) ->
							#{
								name => P,
								in => path,
								required => true,
								type => string
							}
						end,
						maps:get(url_params, Req)
					),
					AuthenicationParams = case maps:get(authenticate, Req) of
						true -> [#{
							name => 'Token',
							in => header,
							required => true,
							type => string,
							description => <<"API Access Token">>
						}];
						false -> []
					end,
					AuthorizationParams = case maps:get(authorize, Req) of
						true -> [#{
							name => 'Admin-Key',
							in => header,
							required => true,
							type => string,
							description => <<"Admin API Access Token">>
						}];
						false -> []
					end,
					PaginationParams = case maps:get(paginate, Req) of
						"pagination_args" -> [
							#{
								name => 'start_time',
								in => 'query',
								required => true,
								type => integer,
								description => <<"Start Time">>
							},
							#{
								name => 'end_time',
								in => 'query',
								required => true,
								type => integer,
								description => <<"End Time">>
							},
							#{
								name => 'max_result',
								in => 'query',
								required => false,
								type => integer,
								description => <<"Max Result (defualt=10, max=15)">>
							},
							#{
								name => 'continuation',
								in => 'query',
								required => false,
								type => string,
								description => <<"Continuation">>
							}
						];
						"" -> []
					end,
					MainStatusCode = integer_to_binary(maps:get(response_status_code, Req)),
					MainMessage = case maps:get(response_body, Req) of
						'' -> #{
							MainStatusCode => #{description => ''}
						};
						X -> #{
							MainStatusCode => #{
								description => '',
								schema => #{
									<<"$ref">> => list_to_binary("#/definitions/" ++ atom_to_list(X))
								}
							}
						}
					end,
					ResponseMessages = maps:from_list(
						lists:map(
							fun({StatusCode, Desc}) ->
								{integer_to_binary(StatusCode), #{description => Desc}}
							end,
							maps:get(response_messages, Req)
						)
					),
					RequestInfo = #{
						summary => list_to_binary(maps:get(name, Req)),
						tags => [list_to_binary(Tag)],
						parameters => lists:append(
							lists:append(
								lists:append(Params, UrlParams),
								lists:append(AuthenicationParams, AuthorizationParams)
							),
							PaginationParams
						),
						responses => maps:merge(MainMessage, ResponseMessages)
					},

					Method = list_to_binary(string:to_lower(atom_to_list(maps:get(method, Req)))),
					UpdatedR = maps:put(Method, RequestInfo, R),
					Res#{Url => UpdatedR}
				end,
				Result,
				ReqList
			)
		end,
		#{},
		Routes
	),

	#{
		<<"swagger">>		=> <<"2.0">>,
		<<"info">>			=> Info,
		<<"basePath">>		=> <<"/api">>,
		<<"consumes">>		=> [<<"application/json">>],
		<<"produces">>		=> [<<"application/json">>],
		<<"definitions">>	=> Definitions,
		<<"paths">>			=> Paths
	}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

get_routes() ->
	Cwd = filename:join(filename:dirname(code:which(?MODULE)), "../"),
	IncludeDir = filename:join(Cwd, "include"),
	Sources = filelib:wildcard(filename:join(Cwd, "src/api/*.erl")),

	Resources = lists:map(
		fun(Source) ->
			Name = filename:basename(Source),
			string:substr(Name, 12,  length(Name) - 26)
		end,
		Sources
	),

	Abstracts = lists:map(
		fun(S) ->
			{ok, Abstract} = epp:parse_file(S, [{includes, [IncludeDir]}]),
			Abstract
		end,
		Sources
	),

	Routes = lists:map(fun(Abs) -> get_handler_info(Abs) end, Abstracts),
	lists:zip(Resources, Routes).


get_handler_info(HandlerAbstract) ->
	AbsClauses = lists:filtermap(
		fun(Line) ->
			case erlang:element(1, Line) =:= function of
				true ->
					Function = atom_to_list(erlang:element(3, Line)),
					RF = re:run(Function, "^handle_.*"),
					case RF of
						{match, _} -> {true, {re:replace(Function, "^handle_", "", [global, {return,list}]),
											  lists:nth(1, erlang:element(5, Line))}};
						nomatch -> false
					end;
				false -> false
			end
		end,
		HandlerAbstract
	),

	Routes = lists:map(
		fun(Abs) ->
			{Name, Clause} = Abs,
			[AbsMethod, AbsUrl | _] = erlang:element(3, Clause),
			Method = erlang:element(3, AbsMethod),
			Url = lists:droplast("/" ++ get_url(AbsUrl)),
			UrlParams = get_url_params(AbsUrl),
			AbsBody = erlang:element(5, Clause),
			Authenticate = case find_authenticate(AbsBody) of
				[] -> false;
				[true] -> true
			end,
			Authorize = case find_authorize(AbsBody) of
				[] -> false;
				[true] -> true
			end,
			Paginate = case find_paginate(AbsBody) of
				[] -> "";
				[P] -> atom_to_list(P)
			end,
			Schema = case find_schema(AbsBody) of
				[] -> "";
				[S] -> atom_to_list(S)
			end,
			Aborts = find_aborts(AbsBody),
			{ResponseBody, StatusCode} = find_result(AbsBody),
			#{name => Name,
			  method => Method,
			  url => Url,
			  url_params => UrlParams,
			  reqeust_body => Schema,
			  authenticate => Authenticate,
			  authorize => Authorize,
			  paginate => Paginate,
			  response_body => ResponseBody,
			  response_status_code => StatusCode,
			  response_messages => Aborts}
		end,
		AbsClauses
	),
	Routes.


get_url(AbsUrl) ->
	case erlang:element(1, AbsUrl) of
		cons ->
			get_url(erlang:element(3, AbsUrl)) ++ "/" ++ get_url(erlang:element(4, AbsUrl));
		nil -> "";
		string -> erlang:element(3, AbsUrl);
		var -> "{" ++ atom_to_list(erlang:element(3, AbsUrl)) ++ "}"
	end.


get_url_params(AbsUrl) ->
	case erlang:element(1, AbsUrl) of
		cons ->
			lists:append(
				get_url_params(erlang:element(3, AbsUrl)),
				get_url_params(erlang:element(4, AbsUrl))
			);
		nil -> [];
		string -> [];
		var -> [erlang:element(3, AbsUrl)]
	end.


find_result(AbsBody) ->
	{tuple, _, [Abs, {integer, _, StatusCode}]} = lists:nth(erlang:length(AbsBody), AbsBody),
	Body = case Abs of
		{string, _, X} -> list_to_atom(X);
		{atom, _, X} -> X;
		{bin, _, [{bin_element, _, {string, _, X}, _, _}]} -> list_to_atom(X);
		{record, _, {var, _, _}, X, _} -> X;
		{record, _, X, _} -> X
	end,
	{Body, StatusCode}.


find_authenticate(AbsBody) ->
	lists:filtermap(
		fun(Line) ->
			case erlang:element(1, Line) of
				call ->
					Call = erlang:element(3, Line),
					case erlang:element(1, Call) =:= remote of
						true ->
							case erlang:element(3, erlang:element(4, Call)) =:= authenticate of
								true -> {true, true};
								false -> false
							end;
						false -> false
					end;
				match ->
					case erlang:element(1, erlang:element(4, Line)) =:= call of
						true ->
							Call = erlang:element(3, erlang:element(4, Line)),
							case erlang:element(1, Call) =:= remote of
								true ->
									case erlang:element(3, erlang:element(4, Call)) =:= authenticate of
										true -> {true, true};
										false -> false
									end;
								false -> false
							end;
						false -> false
					end;
				_ -> false
			end
		end,
		AbsBody
	).


find_authorize(AbsBody) ->
	lists:filtermap(
		fun(Line) ->
			case erlang:element(1, Line) of
				call ->
					Call = erlang:element(3, Line),
					case erlang:element(1, Call) =:= remote of
						true ->
							case erlang:element(3, erlang:element(4, Call)) =:= authorize of
								true -> {true, true};
								false -> false
							end;
						false -> false
					end;
				_ -> false
			end
		end,
		AbsBody
	).


find_paginate(AbsBody) ->
	lists:filtermap(
		fun(Line) ->
			case erlang:element(1, Line) of
				call ->
					Call = erlang:element(3, Line),
					case erlang:element(1, Call) =:= remote of
						true ->
							case erlang:element(3, erlang:element(4, Call)) of
								pagination_args -> {true, pagination_args};
								_ -> false
							end;
						false -> false
					end;
				match ->
					case erlang:element(1, erlang:element(4, Line)) =:= call of
						true ->
							Call = erlang:element(3, erlang:element(4, Line)),
							case erlang:element(1, Call) =:= remote of
								true ->
									case erlang:element(3, erlang:element(4, Call)) of
										pagination_args -> {true, pagination_args};
										_ -> false
									end;
								false -> false
							end;
						false -> false
					end;
				_ -> false
			end
		end,
		AbsBody
	).


find_schema(AbsBody) ->
	lists:filtermap(
		fun(Line) ->
			case erlang:element(1, Line) =:= call of
				true ->
					Call = erlang:element(3, Line),
					case erlang:element(1, Call) =:= remote of
						true ->
							case erlang:element(3, erlang:element(4, Call)) =:= validate_schema of
								true ->
									[{_, _, Schema}] = erlang:element(4, Line),
									{true, Schema};
								false -> false
							end;
						false -> false
					end;
				false -> false
			end
		end,
		AbsBody
	).


find_aborts(AbsBody) ->
	Aborts = lists:filtermap(
		fun(Line) ->
			case erlang:element(1, Line) of
				call ->
					Call = erlang:element(3, Line),
					case erlang:element(1, Call) =:= remote of
						true ->
							case erlang:element(3, erlang:element(4, Call)) =:= abort of
								true ->
									case erlang:element(4, Line) of
										[{integer, _, StatusCode}] -> {true, {StatusCode, <<"">>}};
										[{integer, _, StatusCode}, {string, _, Reason}] -> {true, {StatusCode, list_to_binary(Reason)}};
										[{integer, _, StatusCode}, {atom, _, Reason}] -> {true, {StatusCode, Reason}};
										[{integer, _, StatusCode}, {bin, _, [{_, _, {string, _, Reason}, _, _}]}] -> {true, {StatusCode, list_to_binary(Reason)}}
									end;
								false -> false
							end;
						false -> false
					end;
				match ->
					X = find_aborts([erlang:element(4, Line)]),
					case X =:= [] of
						false -> {true, X};
						true -> false
					end;
				'case' ->
					X = find_aborts(erlang:element(4, Line)),
					case X =:= [] of
						false -> {true, X};
						true -> false
					end;
				clause ->
					X = find_aborts(erlang:element(5, Line)),
					case X =:= [] of
						false -> {true, X};
						true -> false
					end;
				'try' ->
					X1 = find_aborts(erlang:element(4, Line)),
					X2 = find_aborts(erlang:element(5, Line)),
					X = lists:merge(X1, X2),
					case X =:= [] of
						false -> {true, X};
						true -> false
					end;
				_ -> false
			end
		end,
		AbsBody
	),
	lists:foldr(
		fun(A, Res) ->
			case is_list(A) of
				true -> lists:append(Res, A);
				false -> lists:append(Res, [A])
			end
		end,
		[],
		Aborts
	).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

get_schemas() ->
	Cwd = filename:join(filename:dirname(code:which(?MODULE)), "../"),
	Headers = filelib:wildcard(filename:join(Cwd, "include/**/*.hrl")),
	Abstracts = lists:map(fun(H) -> edoc:read_source(H) end, Headers),

	CleanAbstracts = lists:foldr(
		fun(Abs, List) ->
			lists:foldr(
				fun(Abs2, List2) -> [Abs2 | List2] end,
				List,
				Abs)
		end,
		[],
		Abstracts
	),

	AbsRecords = lists:filtermap(
		fun(Abs) ->
			case erlang:element(3, Abs) =:= record of
				true -> {true, erlang:element(4, Abs)};
				false -> false
			end
		end,
		CleanAbstracts
	),

	lists:map(fun(Abs) -> get_record_info(Abs) end, AbsRecords).


get_record_info(RecordAbstract) ->
	SchemaName = erlang:element(1, RecordAbstract),
	AbsFields = lists:map(
		fun(F) ->
			erlang:element(2, F)
		end,
		erlang:element(2, RecordAbstract)
	),
	AbsTypes = lists:map(
		fun(F) ->
			lists:nth(2,erlang:element(4, erlang:element(3, F)))
		end,
		erlang:element(2, RecordAbstract)
	),

	Fields = lists:map(
		fun(F) ->
			erlang:element(3, erlang:element(3, F))
		end,
		AbsFields
	),

	Types = lists:map(
		fun(T) ->
			get_type(T)
		end,
		AbsTypes
	),

	SchemaInfo = lists:zip(Fields, Types),
	{SchemaName, SchemaInfo}.


get_type(AbsType) ->
	case erlang:element(3, AbsType) of
		list ->
			[T] = erlang:element(4, AbsType),
			[get_type(T)];
		record ->
			[Rec] = erlang:element(4, AbsType),
			{schema, erlang:element(3, Rec)};
		string -> string;
		binary -> string;
		integer -> integer;
		number -> number;
		boolean -> boolean
	end.
