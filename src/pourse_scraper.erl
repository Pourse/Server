-module(pourse_scraper).

-export([
	scrape_categories/0,
	scrape_stock_general_info/1,
	scrape_comments/2,
	scrape_news_list/1,
	scrape_news/1,
	scrape_bourse_status/0
]).

-import(pourse_json_helper, [
	from_json/2
]).


scrape_categories() ->
	Res = pourse_python:call(scraper, scrape_categories, []),
	from_json(Res, map).


scrape_stock_general_info(StockIc) ->
	Res = pourse_python:call(scraper, scrape_stock_general_info, [StockIc]),
	from_json(Res, map).


scrape_comments(ToDate, FromIndex) ->
	Res = pourse_python:call(scraper, scrape_comments, [ToDate, FromIndex]),
	from_json(Res, map).


scrape_news_list(Count) ->
	Res = pourse_python:call(scraper, scrape_news_list, [Count]),
	from_json(Res, map).


scrape_news(Url) ->
	Res = pourse_python:call(scraper, scrape_news, [Url]),
	from_json(Res, map).


scrape_bourse_status() ->
	Res = pourse_python:call(scraper, scrape_bourse_status, []),
	from_json(Res, map).
