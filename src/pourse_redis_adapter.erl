-module(pourse_redis_adapter).

-export([
	new/0,
	setex/4,
	set/3,
	get/2,
	del/2
]).

-include("config.hrl").


new() ->
	{ok, Con} = eredis:start_link(?env(redis, host), ?env(redis, port)),
	{?MODULE, Con}.


setex(Key, Value, Expire, {?MODULE, Con}) ->
	{ok, <<"OK">>} = eredis:q(Con, ["SETEX", Key, Expire, Value]),
	ok.


set(Key, Value, {?MODULE, Con}) ->
	{ok, <<"OK">>} = eredis:q(Con, ["SET", Key, Value]),
	ok.


get(Key, {?MODULE, Con}) ->
	{ok, Value} = eredis:q(Con, ["GET", Key]),
	Value.


del(Key, {?MODULE, Con}) ->
	{ok, _} = eredis:q(Con, ["DEL", Key]),
	ok.
