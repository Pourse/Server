-module(pourse_validator).

-export([
	validate_schema/1
]).

-include_lib("eunit/include/eunit.hrl").
-include("api/api.hrl").
-include("api/form/login_form.hrl").


validate_schema(login_form) ->
	Json = ?request(json),
	?assertNotEqual(none, Json),
	login_form = erlang:element(1, Json),
	uuid:to_binary(binary_to_list(Json#login_form.onesignal_id)), % validation
	ok;


validate_schema(Schema) ->
	Json = ?request(json),
	?assertNotEqual(none, Json),
	Schema = erlang:element(1, Json),
	ok.
