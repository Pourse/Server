-module(pourse).

-export([
	start/0,
	start_jobqueue_node/0,
	get_env/0,
	get_env/1,
	get_env/2,
	upgrade_code/0
]).


start() ->
	ok = application:load(pourse),
	ok = init_env(),
	ok = application:start(sasl),
	ok = pourse_node_manager:start(),
	ok = pourse_db_adapter:start(),
	{ok, _} = pourse_jobqueue_sup:start_link(),
	ok = pourse_json_helper:start(),
	ok = pourse_http_receiver:start(),
	ok = pourse_manager:start(),
	ok = pourse_doc_generator:start(),
	ok.


start_jobqueue_node() ->
	ok = application:load(pourse),
	ok = application:start(inets),
	ok = ssl:start(),
	ok = pourse_db_adapter:start(),
	ok = pourse_jobqueue:start(),
	ok = pourse_json_helper:start(),
	ok.


init_env() ->
	{_, Dirs} = lists:unzip(get_env(dir)),
	lists:foreach(
		fun(Dir) ->
			filelib:ensure_dir(Dir),
			file:make_dir(Dir)
		end,
		Dirs
	),
	ok.



get_env() ->
	application:get_all_env(pourse).


get_env(Tag) ->
	{ok, List} = application:get_env(pourse, Tag),
	List.


get_env(Tag, Key) ->
	proplists:get_value(Key, get_env(Tag)).



upgrade_code() ->
	Dir = filename:dirname(code:which(?MODULE)),
	os:cmd("make -C " ++ filename:dirname(Dir)),
	{ok, Modules} = file:list_dir(Dir),
	lists:foreach(
		fun(X) ->
			case lists:suffix(".beam", X) of
				false -> void;
				true ->
					ModName = list_to_atom(string:substr(X, 1,  length(X) - 5)),
					case proplists:is_defined(upgrade_code, ModName:module_info(exports)) of
						false ->
							code:purge(ModName),
            				code:load_file(ModName);
            			true ->
            				ModName:upgrade_code()
					end
			end
		end,
		lists:delete(atom_to_list(?MODULE) ++ ".beam", Modules)
	).
